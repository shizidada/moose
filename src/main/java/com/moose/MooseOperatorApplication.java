package com.moose;

import com.corundumstudio.socketio.SocketIOServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author taohua
 */
@Slf4j
@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
public class MooseOperatorApplication implements CommandLineRunner {

  @Autowired
  private SocketIOServer socketIOServer;

  public static void main(String[] args) {
    System.setProperty("es.set.netty.runtime.available.processors", "false");
    SpringApplication.run(MooseOperatorApplication.class, args);
  }

  @Override
  public void run(String... strings) {
    socketIOServer.start();
    log.info("socket.io 启动成功");
  }
}
