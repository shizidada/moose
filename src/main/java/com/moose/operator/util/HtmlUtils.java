package com.moose.operator.util;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.owasp.html.ElementPolicy;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;

/**
 * @author taohua
 */
@Slf4j
public class HtmlUtils {

  /**
   * 允许的标签
   */
  private static final String[] ALLOWED_TAGS = {"h1", "h2", "h3", "h4", "h5", "h6",
      "span", "strong",
      "img", "video", "source",
      "blockquote", "p", "div",
      "ul", "ol", "li",
      "table", "thead", "caption", "tbody", "tr", "th", "td", "br",
      "a"
  };

  /**
   * 允许存在的属性 attributeNames
   */
  private static final String[] ALLOW_ATTRIBUTES = {"src", "href", "target", "class", "id"};

  /**
   * 需要转化的标签
   */
  private static final String[] NEED_TRANSFORM_TAGS =
      {"article", "aside", "command", "datalist", "details", "figcaption", "figure",
          "footer", "header", "hgroup", "section", "summary"};

  public static String sanitizeHtml(String htmlContent) {
    PolicyFactory policy = new HtmlPolicyBuilder()
        // 所有允许的标签
        .allowElements(ALLOWED_TAGS)
        // 内容标签转化为div
        .allowElements(new ElementPolicy() {
          @Override
          public String apply(String elementName, List<String> attributes) {
            return "span";
          }
        }, NEED_TRANSFORM_TAGS)
        .allowAttributes(ALLOW_ATTRIBUTES).onElements(ALLOWED_TAGS)
        // 校验链接中的是否为 https
        .allowUrlProtocols("https")
        .toFactory();
    return policy.sanitize(htmlContent);
  }

  public static void main(String[] args) {
    String html = null;
    html =
        "<th id=\"thisId\" class=\"favorite_table_column\"><span class=\"ant-table-header-column\"><div><span class=\"ant-table-column-title\">类型</span><span class=\"ant-table-column-sorter\"></span></div></span></th>";
    //html = "<div><script>alert('test');</script><div>";
    //html = "<input type=\"button\" value=\"1\" onclick=\"alert(document.cookie)\" />";
    //html =
    //    "<script> var body= document.body; var img = document.createElement(\"img\"); img.setAttribute('style','width:100%;height:100%;z-index:99999;position:fixed;top:0px;') img.src = \"https://www.baidu.com/img/bd_logo1.png\"; body.appendChild(img); </script>";
    html =
        "<p>二改，根据读者反馈哈，我来补点东西。</p><p>————————————</p><p>今天就回来修改一下，再增加点东西。</p><p>";
    log.info(sanitizeHtml(html));
  }
}
