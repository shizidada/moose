package com.moose.operator.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.MatchMode;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PolicyConditions;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.moose.operator.configure.properties.AliYunOSSProperties;
import com.moose.operator.constant.AliYunOSSConstant;
import com.moose.operator.constant.CommonConstant;
import com.moose.operator.model.api.ResultCode;
import com.moose.operator.model.dto.FileUploadInfoDTO;
import com.moose.operator.web.service.UserInfoService;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-10-14 22:26:22:26
 * @see com.moose.operator.util
 */
@Slf4j
public class OSSClientUtils {

  private AliYunOSSProperties aliYunOssProperties;

  private UserInfoService userInfoService;

  public void setUserInfoService(UserInfoService userInfoService) {
    this.userInfoService = userInfoService;
  }

  public void setAliYunOssProperties(AliYunOSSProperties aliYunOssProperties) {
    this.aliYunOssProperties = aliYunOssProperties;
  }

  private OSS getOssClient() {
    // 创建OSSClient实例。
    return new OSSClientBuilder().build(aliYunOssProperties.getEndpoint(),
        aliYunOssProperties.getAccessKeyId(),
        aliYunOssProperties.getAccessKeySecret());
  }

  public FileUploadInfoDTO uploadFile(MultipartFile file) {
    return this.uploadFile(file, AliYunOSSConstant.ROOT_BUCKET_NAME_KEY,
        file.getOriginalFilename());
  }

  /**
   * 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。 强烈建议您创建并使用RAM账号进行API访问或日常运维， 请登录
   * https://ram.console.aliyun.com 创建RAM账号。
   */
  public FileUploadInfoDTO uploadFile(MultipartFile file, String bucketName, String fileName) {
    // 创建OSSClient实例。
    OSS ossClient = this.getOssClient();

    FileUploadInfoDTO fileUploadInfoDTO = new FileUploadInfoDTO();

    // 创建PutObjectRequest对象。
    try {
      InputStream inputStream = file.getInputStream();
      Long currentUserId = userInfoService.getCurrentUserId();

      String uuid = UUID.randomUUID().toString();
      String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
      String dateFormat = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
      // 用户上传文件时指定的前缀。
      String fileDir = bucketName + currentUserId + "/" + dateFormat + "/" + uuid + "." + suffix;

      PutObjectRequest putObjectRequest =
          new PutObjectRequest(aliYunOssProperties.getBucketName(), fileDir, inputStream);

      ObjectMetadata objectMetadata = new ObjectMetadata();
      objectMetadata.setObjectAcl(CannedAccessControlList.PublicRead);
      putObjectRequest.setMetadata(objectMetadata);
      PutObjectResult putObjectResult = ossClient.putObject(putObjectRequest);
      String fileUrl = "https://moose-plus.oss-cn-shenzhen.aliyuncs.com/" + fileDir;
      fileUploadInfoDTO.setFileUrl(fileUrl);
      fileUploadInfoDTO.setTag(putObjectResult.getETag());
      fileUploadInfoDTO.setSuccess(CommonConstant.SUCCESS);
    } catch (Exception e) {
      log.error("upload file to oss fail : {}", e.getMessage());
      // throw new BusinessException(message, ResultCode.FILE_UPLOAD_ERROR.getCode());
      // TODO: throw error or other
      String errorMessage = ResultCode.FILE_UPLOAD_ERROR.getMessage();
      String message = String.format("[%s %s]", errorMessage, e.getMessage());
      fileUploadInfoDTO.setSuccess(CommonConstant.FAIL);
      fileUploadInfoDTO.setErrMessage(message);
    } finally {
      // 关闭 OSSClient
      ossClient.shutdown();
    }
    return fileUploadInfoDTO;
  }

  public Map<String, String> getSignature() {

    // 创建OSSClient实例。
    OSS ossClient = this.getOssClient();
    Map<String, String> respMap = null;
    try {
      respMap = new LinkedHashMap<>();

      Long currentUserId = userInfoService.getCurrentUserId();

      String accessId = aliYunOssProperties.getAccessKeyId();
      String endpoint = aliYunOssProperties.getEndpoint();
      String bucketName = aliYunOssProperties.getBucketName();
      // host的格式为 bucketname.endpoint
      String host = "https://" + bucketName + "." + endpoint;
      String dateFormat = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
      // 用户上传文件时指定的前缀。
      String fileDir = currentUserId + "/" + dateFormat + "/";

      long expireTime = 30;
      long expireEndTime = System.currentTimeMillis() + expireTime * 1000;
      Date expiration = new Date(expireEndTime);
      // PostObject 请求最大可支持的文件大小为5 GB，即 CONTENT_LENGTH_RANGE 为5*1024*1024*1024。
      PolicyConditions policyConds = new PolicyConditions();
      policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, 1048576000);
      policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, fileDir);

      String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
      byte[] binaryData = postPolicy.getBytes(StandardCharsets.UTF_8);
      String encodedPolicy = BinaryUtil.toBase64String(binaryData);
      String signature = ossClient.calculatePostSignature(postPolicy);

      // TODO： 需要构建发布外网环境
      //JSONObject jasonCallback = new JSONObject();
      //jasonCallback.put("callbackUrl", "http://localhost:7000/api/v1/file/callback");
      //jasonCallback.put("callbackBody",
      //    "filename=${object}&size=${size}&mimeType=${mimeType}&height=${imageInfo.height}&width=${imageInfo.width}");
      //jasonCallback.put("callbackBodyType", "application/x-www-form-urlencoded");
      //String base64CallbackBody = BinaryUtil.toBase64String(jasonCallback.toString().getBytes());
      //respMap.put("callbackBody", base64CallbackBody);

      respMap.put("accessId", accessId);
      respMap.put("policy", encodedPolicy);
      respMap.put("signature", signature);
      respMap.put("dir", fileDir);
      respMap.put("host", host);
      respMap.put("expire", String.valueOf(expireEndTime / 1000));
    } catch (Exception e) {
      log.error("获取 OSS 签名异常", e);
      respMap = new LinkedHashMap<>();
      respMap.put("message", e.getMessage());
    } finally {
      ossClient.shutdown();
    }
    return respMap;
  }

  public void setObjectAclPublicRead (String key) {
    OSS ossClient = this.getOssClient();
    ossClient.setObjectAcl(aliYunOssProperties.getBucketName(), key,CannedAccessControlList.PublicRead);
  }
}
