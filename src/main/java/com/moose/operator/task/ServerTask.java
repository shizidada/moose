package com.moose.operator.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author taohua
 */

@Slf4j
@Component
public class ServerTask {

  @Scheduled(cron = "0/10 * * * * ?")
  public void websocket() throws Exception {
    //ChatMessageDTO chatMessageDTO = new ChatMessageDTO();
    //chatMessageDTO.setMessage("你吃饭了吗？？");
    //TextMessage textMessage = new TextMessage(MapperUtils.obj2json(chatMessageDTO));
    //mooseMessageWebSocketHandler.sendMessage("785919644501544960", textMessage);
    //log.info("【推送消息】：{}", MapperUtils.obj2json(textMessage));
  }
}