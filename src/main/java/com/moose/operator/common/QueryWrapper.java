package com.moose.operator.common;

import com.moose.operator.constant.PageInfoConstant;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.LimitOperation;
import org.springframework.data.mongodb.core.aggregation.SkipOperation;
import org.springframework.data.mongodb.core.query.Query;

/**
 * @author taohua
 */

public class QueryWrapper {

  public static Query buildQuery() {
    return new Query();
  }

  public static int buildDefaultOffset(Integer pageNum, Integer pageSize) {
    return (pageNum - PageInfoConstant.DEFAULT_PAGE_NUM) * pageSize;
  }

  public static Query buildPageQuery(Integer pageNum, Integer pageSize) {
    int offset = buildDefaultOffset(pageNum, pageSize);
    Query query = new Query();
    query.limit(pageSize).skip(offset);
    return query;
  }

  public static LimitOperation buildLimitOperation(Integer pageSize) {
    return Aggregation.limit(pageSize);
  }

  public static SkipOperation buildSkipOperation(Integer pageNum, Integer pageSize) {
    long offset = buildDefaultOffset(pageNum, pageSize);
    return Aggregation.skip(offset);
  }
}
