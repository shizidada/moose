package com.moose.operator.common;

import com.moose.operator.constant.PageInfoConstant;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-19 12:29:12:29
 * @see com.moose.operator.util
 */
public class PageInfo<T> implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 当前页
   */
  private Integer pageNum = PageInfoConstant.DEFAULT_PAGE_NUM;

  /**
   * 每页的数量
   */
  private Integer pageSize = PageInfoConstant.DEFAULT_PAGE_SIZE;

  /**
   * 总数
   */
  private long totalSize;
  /**
   * 结果集
   */
  private List<T> list;

  public PageInfo() {
  }

  public Integer getPageNum() {
    return pageNum;
  }

  public void setPageNum(Integer pageNum) {
    if (null != pageNum && pageNum != 0) {
      this.pageNum = pageNum;
    }
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    if (null != pageSize && pageSize != 0) {
      this.pageSize = pageSize;
    }
  }

  public long getTotalSize() {
    return totalSize;
  }

  public void setTotalSize(long totalSize) {
    this.totalSize = totalSize;
  }

  public List<T> getList() {
    return list;
  }

  public void setList(List<T> list) {
    this.list = list;
  }
}
