package com.moose.operator.constant;

/**
 * <p>
 * Description: 匿名访问 url
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-10-17 10:30:10:30
 * @see com.moose.operator.constant
 */
public interface AnonymousUrlConstant {

  String LOGIN_IN_URL = "/api/v1/account/login";

  String LOGIN_OUT_URL = "/api/v1/account/logout";

  String REGISTER_URL = "/api/v1/account/register";

  String LOGIN_STATUS_URL = "/api/v1/account/status";

  String REFRESH_TOKEN_URL = "/api/v1/token/refresh";

  String SEND_SMS_CODE_URL = "/api/v1/sms/send";

  String DYNAMIC_RECOMMEND_LIST = "/api/v1/dynamic/recommend/list";

  String DYNAMIC_DETAIL = "/api/v1/dynamic/detail/*";

  String ID_URL = "/api/v1/id/**";

  String TEST_URL = "/api/v1/test/**";
}
