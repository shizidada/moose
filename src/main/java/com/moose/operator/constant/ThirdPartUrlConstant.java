package com.moose.operator.constant;

/**
 * @author taohua
 */

public interface ThirdPartUrlConstant {

  String GITHUB_ACCESS_TOKEN_URL = "https://github.com/login/oauth/access_token";

  String GITHUB_USER_INFO_URL = "https://api.github.com/user";
}
