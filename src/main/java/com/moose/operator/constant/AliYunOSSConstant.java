package com.moose.operator.constant;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-10-14 22:20:22:20
 * @see com.moose.operator.constant
 */
public interface AliYunOSSConstant {

  String ROOT_BUCKET_NAME_KEY = "";

  String USER_AVATAR_BUCKET_NAME_KEY = "user/avatar/";
}
