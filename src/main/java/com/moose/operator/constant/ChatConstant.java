package com.moose.operator.constant;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-09-20 13:10:13:10
 * @see com.moose.operator.constant
 */
public interface ChatConstant {

  String SINGLE_CHAT = "SINGLE_CHAT";

  String GROUP_CHAT = "GROUP_CHAT";

  /**
   * 未读
   */
  Integer MESSAGE_UN_READ = 1;

  /**
   * 已读
   */
  Integer MESSAGE_IS_READ = 2;
}
