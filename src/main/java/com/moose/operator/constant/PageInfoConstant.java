package com.moose.operator.constant;

/**
 * @author taohua
 */

public interface PageInfoConstant {

  Integer DEFAULT_PAGE_NUM = 1;

  Integer DEFAULT_MIN_PAGE_SIZE = 1;

  Integer DEFAULT_PAGE_SIZE = 10;
}
