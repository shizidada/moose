package com.moose.operator.constant;

/**
 * @author taihua
 */
public interface SmsTypeConstant {

  String REGISTER = "register";

  String SMS_LOGIN = "sms_login";

  String RESET_PHONE = "reset_phone";

  String RESET_PASSWORD = "reset_password";
}

