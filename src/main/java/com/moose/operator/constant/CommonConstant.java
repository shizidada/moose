package com.moose.operator.constant;

/**
 * @author taohua
 */
public interface CommonConstant {

  /**
   * success
   */
  Integer SUCCESS = 1;

  /**
   * fail
   */
  Integer FAIL = 0;

  /**
   * post method
   */
  String HTTP_METHOD_POST = "POST";

  /**
   * get method
   */
  String HTTP_METHOD_GET = "GET";

  /**
   * attachment size
   */
  Integer UPLOAD_ATTACHMENT_SIZE = 6;

  /**
   * 发送短信验证码 或 验证短信验证码时，传递手机号的参数的名称
   */
  String DEFAULT_PARAMETER_NAME_PHONE = "phone";

  String DEFAULT_PARAMETER_NAME_SMS_TYPE = "type";

  /**
   * 验证短信验证码时，http请求中默认的携带短信验证码信息的参数的名称
   */
  String DEFAULT_PARAMETER_NAME_CODE_SMS = "smsCode";

  String AUTHORIZATION_PARAM = "Authorization";

  String DEFAULT_PARAMETER_NAME_CODE_GITHUB = "code";

  Integer LENGTH_TOW = 2;

  String QR_TICKET = "m_ticket";
}
