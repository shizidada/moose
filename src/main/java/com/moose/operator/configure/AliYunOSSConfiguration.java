package com.moose.operator.configure;

import com.moose.operator.configure.properties.AliYunOSSProperties;
import com.moose.operator.util.OSSClientUtils;
import com.moose.operator.web.service.UserInfoService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-10-14 22:35:22:35
 * @see com.moose.operator.configure
 */
@Configuration
public class AliYunOSSConfiguration {

  @Bean
  public OSSClientUtils ossClientUtils(UserInfoService userInfoService,
      AliYunOSSProperties aliYunOssProperties) {
    OSSClientUtils ossClientUtils = new OSSClientUtils();
    ossClientUtils.setUserInfoService(userInfoService);
    ossClientUtils.setAliYunOssProperties(aliYunOssProperties);
    return ossClientUtils;
  }
}
