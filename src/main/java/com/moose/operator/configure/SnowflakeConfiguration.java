package com.moose.operator.configure;

import com.moose.operator.common.SnowflakeIdWorker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author taohua
 */
@Configuration
public class SnowflakeConfiguration {

  @Bean
  public SnowflakeIdWorker snowflakeIdWorker() {
    return new SnowflakeIdWorker(1, 2);
  }
}
