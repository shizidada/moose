package com.moose.operator.exception;

import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-11-08 20:50:20:50
 * @see com.moose.operator.exception
 */
@Slf4j
@Component
public class MooseDefaultErrorAttributes extends DefaultErrorAttributes {

  @Override public Map<String, Object> getErrorAttributes(WebRequest webRequest,
      ErrorAttributeOptions options) {
    Map<String, Object> errors = super.getErrorAttributes(webRequest, options);
    Throwable throwable = super.getError(webRequest);
    if (null == throwable) {
      log.error("[系统捕获全局异常] {}", errors);
      HashMap<String, Object> errorsMap = new HashMap<>(16);
      Integer status = (Integer) errors.get("status");
      String error = (String) errors.get("error");
      errorsMap.put("code", status);
      errorsMap.put("message", error);
      errorsMap.put("data", null);
      return errorsMap;
    }
    return errors;
  }
}
