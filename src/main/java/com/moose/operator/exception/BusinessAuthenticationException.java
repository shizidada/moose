package com.moose.operator.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moose.operator.model.api.ResultCode;
import com.moose.operator.web.security.component.MooseAuthenticationExceptionSerializer;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-07-29 23:08:23:08
 * @see com.moose.operator.web.security.component
 */
@JsonSerialize(using = MooseAuthenticationExceptionSerializer.class)
public class BusinessAuthenticationException extends OAuth2Exception {
  private String message;
  private Integer code;

  public BusinessAuthenticationException(String message, Integer code) {
    super(message);
    this.message = message;
    this.code = code;
  }

  public BusinessAuthenticationException(ResultCode resultCode) {
    super(resultCode.getMessage());
    this.message = resultCode.getMessage();
    this.code = resultCode.getCode();
  }

  @Override public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }
}
