package com.moose.operator.socket.io;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.moose.operator.constant.ChatConstant;
import com.moose.operator.model.template.MessageTemplate;
import com.moose.operator.web.service.SingleMessageService;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-01-26 20:00:20:00
 * @see com.moose.operator.socket.io
 */

@Component
@Slf4j
public class SingleMessageHandler {

  @Resource
  private SingleMessageService singleMessageService;

  /**
   * 客户端事件  SINGLE_CHAT
   *
   * @param client   　客户端信息
   * @param request  请求信息
   * @param template 　客户端发送数据
   */
  @OnEvent(value = ChatConstant.SINGLE_CHAT)
  public void onSingleChat(SocketIOClient client, AckRequest request, MessageTemplate template) {
    log.info("接收消息: 监听事件[{}] 消息 [{}]", ChatConstant.SINGLE_CHAT, template);
    singleMessageService.saveMessage(template);
  }
}
