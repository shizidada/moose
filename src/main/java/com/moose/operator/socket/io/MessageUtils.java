package com.moose.operator.socket.io;

import com.moose.operator.model.emun.ChatTypeEnum;
import com.moose.operator.model.emun.MessageTypeEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-01-26 20:31:20:31
 * @see com.moose.operator.socket.io
 */
@Slf4j
public class MessageUtils {

  /**
   * 未知
   */
  public static final int MESSAGE_TYPE_UN_KNOW = 0;
  /**
   * 文本消息
   */
  public static final int MESSAGE_TYPE_TEXT = 1;
  /**
   * 图片
   */
  public static final int MESSAGE_TYPE_IMAGE = 2;
  /**
   * 文件
   */
  public static final int MESSAGE_TYPE_FILE = 3;
  /**
   * 位置
   */
  public static final int MESSAGE_TYPE_LOCATION = 4;
  /**
   * 视频
   */
  public static final int MESSAGE_TYPE_VIDEO = 5;
  /**
   * 音频
   */
  public static final int MESSAGE_TYPE_VOICE = 6;

  //////////////////////////////////////////////////////////////////////////////////////////

  public static final int CHAT_TYPE_UN_KNOW = 0;
  /**
   * 单聊
   */
  public static final int CHAT_TYPE_SINGLE = 1;
  /**
   * 群聊
   */
  public static final int CHAT_TYPE_GROUP = 2;

  private MessageUtils() {
  }

  /**
   * 转换消息类型 存入数据库
   *
   * @param type 消息类型
   * @return 数据库格式
   */
  public static Integer convertType(String type) {
    if (MessageTypeEnum.TEXT.getValue().equals(type)) {
      return MESSAGE_TYPE_TEXT;
    } else if (MessageTypeEnum.IMAGE.getValue().equals(type)) {
      return MESSAGE_TYPE_IMAGE;
    } else if (MessageTypeEnum.FILE.getValue().equals(type)) {
      return MESSAGE_TYPE_FILE;
    } else if (MessageTypeEnum.LOCATION.getValue().equals(type)) {
      return MESSAGE_TYPE_LOCATION;
    } else if (MessageTypeEnum.VIDEO.getValue().equals(type)) {
      return MESSAGE_TYPE_VIDEO;
    } else if (MessageTypeEnum.VOICE.getValue().equals(type)) {
      return MESSAGE_TYPE_VOICE;
    }
    return MESSAGE_TYPE_UN_KNOW;
  }

  public static String convertType(Integer type) {
    if (type == MESSAGE_TYPE_TEXT) {
      return MessageTypeEnum.TEXT.getValue();
    }
    if (type == MESSAGE_TYPE_IMAGE) {
      return MessageTypeEnum.IMAGE.getValue();
    }
    if (type == MESSAGE_TYPE_FILE) {
      return MessageTypeEnum.FILE.getValue();
    }
    if (type == MESSAGE_TYPE_LOCATION) {
      return MessageTypeEnum.LOCATION.getValue();
    }
    if (type == MESSAGE_TYPE_VIDEO) {
      return MessageTypeEnum.VIDEO.getValue();
    }
    if (type == MESSAGE_TYPE_VOICE) {
      return MessageTypeEnum.VOICE.getValue();
    }
    return MessageTypeEnum.UN_KNOW.getValue();
  }

  /**
   * 转换聊天类型 存入数据库
   *
   * @param chatType 聊天类型
   * @return 数据库格式
   */
  public static Integer convertChatType(String chatType) {
    if (ChatTypeEnum.SINGLE.getValue().equals(chatType)) {
      return 1;
    } else if (ChatTypeEnum.GROUP.getValue().equals(chatType)) {
      return 2;
    }
    return 0;
  }

  public static String convertChatType(int chatType) {
    if (chatType == CHAT_TYPE_SINGLE) {
      return ChatTypeEnum.SINGLE.getValue();
    } else if (chatType == CHAT_TYPE_GROUP) {
      return ChatTypeEnum.GROUP.getValue();
    }
    return ChatTypeEnum.UN_KNOW.getValue();
  }
}
