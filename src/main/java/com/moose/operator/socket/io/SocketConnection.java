package com.moose.operator.socket.io;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-01-23 08:39:08:39
 * @see com.moose.operator.socket.io
 */

@Component
@Slf4j
public class SocketConnection {
  public static ConcurrentMap<String, SocketIOClient> socketIOClientMap = new ConcurrentHashMap<>();

  /**
   * 客户端连接的时候触发
   *
   * @param client SocketIOClient
   */
  @OnConnect
  public void onConnect(SocketIOClient client) {
    String sessionId = client.getHandshakeData().getSingleUrlParam("userId");
    if (!StringUtils.isEmpty(sessionId)) {
      // 存储 SocketIOClient，用于发送消息
      socketIOClientMap.put(sessionId, client);
      log.info("用户:" + sessionId + "已连接");
    }
  }

  /**
   * 客户端关闭连接时触发
   *
   * @param client SocketIOClient
   */
  @OnDisconnect
  public void onDisconnect(SocketIOClient client) {
    String sessionId = client.getHandshakeData().getSingleUrlParam("userId");
    if (!StringUtils.isEmpty(sessionId)) {
      socketIOClientMap.remove(sessionId);
      log.info("用户:" + sessionId + "断开连接");
    }
  }

  public SocketIOClient getSocketIOClient(String sessionId) {
    return socketIOClientMap.get(sessionId);
  }

  public ConcurrentMap<String, SocketIOClient> getConnections() {
    return socketIOClientMap;
  }
}
