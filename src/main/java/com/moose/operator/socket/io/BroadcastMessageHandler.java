package com.moose.operator.socket.io;

import com.corundumstudio.socketio.SocketIOClient;
import java.util.concurrent.ConcurrentMap;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-01-26 20:02:20:02
 * @see com.moose.operator.socket.io
 */

@Component
@Slf4j
public class BroadcastMessageHandler {

  @Resource
  private SocketConnection socketConnection;

  /**
   * 广播消息 "广播消息 All"
   */
  public void sendBroadcast(String message) {
    ConcurrentMap<String, SocketIOClient> socketIOClients =
        socketConnection.getConnections();
    for (SocketIOClient client : socketIOClients.values()) {
      if (client.isChannelOpen()) {
        client.sendEvent("Broadcast", message);
      }
    }
  }
}
