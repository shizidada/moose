package com.moose.operator.web.security.component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.moose.operator.exception.BusinessAuthenticationException;
import java.io.IOException;
import java.util.Map;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-07-29 23:08:23:08
 * @see com.moose.operator.web.security.component
 */
public class MooseAuthenticationExceptionSerializer
    extends StdSerializer<BusinessAuthenticationException> {
  protected MooseAuthenticationExceptionSerializer() {
    super(BusinessAuthenticationException.class);
  }

  @Override public void serialize(BusinessAuthenticationException e, JsonGenerator jsonGenerator,
      SerializerProvider serializerProvider) throws IOException {
    jsonGenerator.writeStartObject();
    jsonGenerator.writeNumberField("code", e.getCode());
    jsonGenerator.writeStringField("message", e.getMessage());
    if (e.getAdditionalInformation() != null) {
      for (Map.Entry<String, String> entry : e.getAdditionalInformation().entrySet()) {
        String key = entry.getKey();
        String value = entry.getValue();
        jsonGenerator.writeStringField(key, value);
      }
    }
    jsonGenerator.writeEndObject();
  }
}
