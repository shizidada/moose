package com.moose.operator.web.security.configure;

import com.moose.operator.constant.AnonymousUrlConstant;
import com.moose.operator.util.RedisHelper;
import com.moose.operator.web.filter.MooseCheckHeaderTokenFilter;
import com.moose.operator.web.filter.MooseLoginVerifyLimitFilter;
import com.moose.operator.web.filter.MooseSmsVerifyLimitFilter;
import com.moose.operator.web.security.component.MooseAccessDeniedHandler;
import com.moose.operator.web.security.component.MooseAuthenticationEntryPoint;
import com.moose.operator.web.security.component.MooseAuthenticationFailureHandler;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

/**
 * @author taohua
 */
@Configuration
@EnableResourceServer
public class MooseResourceServerConfiguration extends ResourceServerConfigurerAdapter implements
    InitializingBean {

  private final List<String> anonymousAntPatterns = new ArrayList<>();

  @Resource
  private MooseAuthenticationFailureHandler mooseAuthenticationFailureHandler;

  @Resource
  private MooseAccessDeniedHandler mooseAccessDeniedHandler;

  @Resource
  private MooseAuthenticationEntryPoint mooseAuthenticationEntryPoint;

  @Resource
  private RedisHelper redisHelper;

  @Lazy
  @Resource
  private TokenStore tokenStore;

  @Override public void afterPropertiesSet() throws ServletException {
    anonymousAntPatterns.add(AnonymousUrlConstant.LOGIN_IN_URL);
    anonymousAntPatterns.add(AnonymousUrlConstant.LOGIN_OUT_URL);
    anonymousAntPatterns.add(AnonymousUrlConstant.REGISTER_URL);
    anonymousAntPatterns.add(AnonymousUrlConstant.LOGIN_STATUS_URL);
    anonymousAntPatterns.add(AnonymousUrlConstant.REFRESH_TOKEN_URL);
    anonymousAntPatterns.add(AnonymousUrlConstant.SEND_SMS_CODE_URL);
    anonymousAntPatterns.add(AnonymousUrlConstant.DYNAMIC_RECOMMEND_LIST);
    anonymousAntPatterns.add(AnonymousUrlConstant.DYNAMIC_DETAIL);
    anonymousAntPatterns.add(AnonymousUrlConstant.TEST_URL);
    anonymousAntPatterns.add(AnonymousUrlConstant.ID_URL);
    anonymousAntPatterns.add("/api/v1/auth/getThirdUrl");
    anonymousAntPatterns.add("/api/v1/auth/third/login");
    anonymousAntPatterns.add("/api/v1/qrcode/get");
    anonymousAntPatterns.add("/api/v1/qrcode/ask");
    anonymousAntPatterns.add("/api/v1/qrcode/scanlogin");
  }

  @Override
  public void configure(HttpSecurity http) throws Exception {

    http.authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/**").permitAll();

    // 对登录注册要允许匿名访问
    for (String pattern : anonymousAntPatterns) {
      http.authorizeRequests().antMatchers(pattern).permitAll();
    }

    // add filter
    http
        .addFilterBefore(checkHeaderTokenFilter(), AbstractPreAuthenticatedProcessingFilter.class)
        .addFilterBefore(loginVerifyLimitFilter(), AbstractPreAuthenticatedProcessingFilter.class)
        .addFilterBefore(smsVerifyLimitFilter(), AbstractPreAuthenticatedProcessingFilter.class);

    // 禁用 session
    http
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
        .authorizeRequests().anyRequest().authenticated().and()
        .cors().and()
        .csrf().disable();
  }

  @Bean
  MooseCheckHeaderTokenFilter checkHeaderTokenFilter() {
    return new MooseCheckHeaderTokenFilter(tokenStore, anonymousAntPatterns);
  }

  @Bean
  MooseSmsVerifyLimitFilter smsVerifyLimitFilter() {
    return new MooseSmsVerifyLimitFilter(redisHelper, mooseAuthenticationFailureHandler);
  }

  @Bean
  MooseLoginVerifyLimitFilter loginVerifyLimitFilter() {
    return new MooseLoginVerifyLimitFilter(redisHelper, mooseAuthenticationFailureHandler);
  }

  @Override
  public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
    // 配置资源 ID
    resources.resourceId("app-resources").stateless(true);
    resources.accessDeniedHandler(mooseAccessDeniedHandler);
    resources.authenticationEntryPoint(mooseAuthenticationEntryPoint);
  }
}