package com.moose.operator.web.controller.poetry;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.poetry.PoetryDetailParam;
import com.moose.operator.web.service.PoetryItemDetailService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author taohua
 */

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/poetry")
public class PoetryItemDetailController {

  @Resource
  private PoetryItemDetailService poetryItemDetailService;

  @PostMapping("/item/detail")
  public R<?> poetryList(@Valid PoetryDetailParam param, BindingResult result) {
    return R.ok(poetryItemDetailService.getDetailByPoetryId(param.getPoetryId()));
  }
}
