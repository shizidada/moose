package com.moose.operator.web.controller.im;

import com.moose.operator.model.api.R;
import com.moose.operator.web.service.MessageRecordService;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-01-23 19:50:19:50
 * @see com.moose.operator.web.controller
 */

@RestController
@RequestMapping("/api/v1/message")
public class MessageController {

  @Resource
  private MessageRecordService messageRecordService;

  /**
   * 获取当前登录用户聊天列表
   *
   * @return R<?> 聊天列表
   */
  @PostMapping(value = "/record/list")
  public R<?> getMessageRecordList() {
    return R.ok(messageRecordService.listMessageRecord());
  }

  /**
   * 获取单聊记录 对方 Id
   *
   * @return R<?> 聊天记录
   */
  @PostMapping(value = "/chat/list")
  public R<?> getMessageChatList(
      @RequestParam("chatId") Long chatId,
      @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum
  ) {
    return R.ok(messageRecordService.listMessageChat(chatId, pageNum));
  }
}
