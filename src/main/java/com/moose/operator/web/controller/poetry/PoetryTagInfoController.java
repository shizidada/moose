package com.moose.operator.web.controller.poetry;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.SearchParam;
import com.moose.operator.web.service.PoetryTagInfoService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author taohua
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v1/poetry")
public class PoetryTagInfoController {
  @Resource
  private PoetryTagInfoService poetryTagInfoService;

  @PostMapping("/tag/list")
  public R<?> tagList(@Valid SearchParam searchParam, BindingResult result) {
    return R.ok(poetryTagInfoService.listPoetryTag(searchParam));
  }

  @PostMapping("/tag/list/top10")
  public R<?> tagListTop10() {
    return R.ok(poetryTagInfoService.tagListTop10());
  }
}
