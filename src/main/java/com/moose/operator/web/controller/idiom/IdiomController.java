package com.moose.operator.web.controller.idiom;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.IdiomSearchParam;
import com.moose.operator.web.service.IdiomItemService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author taohua
 */

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/idiom")
public class IdiomController {

  @Resource
  private IdiomItemService idiomItemService;

  @PostMapping("/item/list")
  public R<?> idiomItemList(@Valid IdiomSearchParam searchParam, BindingResult result) {
    return R.ok(idiomItemService.idiomItemList(searchParam));
  }

  @PostMapping("/structure/list")
  public R<?> idiomStructureList(@Valid IdiomSearchParam searchParam, BindingResult result) {
    return R.ok(idiomItemService.idiomStructureList(searchParam));
  }

  @PostMapping("/type/list")
  public R<?> idiomTypeList(@Valid IdiomSearchParam searchParam, BindingResult result) {
    return R.ok(idiomItemService.idiomTypeList(searchParam));
  }
}
