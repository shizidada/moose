package com.moose.operator.web.controller.book;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.book.HistoryBookChapterParam;
import com.moose.operator.web.service.HistoryBookChapterService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author taohua
 */

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/historybook")
public class HistoryBookChapterController {

  @Resource
  private HistoryBookChapterService historyBookChapterService;

  @PostMapping("/chapter/list")
  public R<?> chapterList(@Valid HistoryBookChapterParam searchParam, BindingResult result) {
    return R.ok(historyBookChapterService.getChapterList(searchParam));
  }

  /**
   * 获取 章节详情
   *
   * @return 章节详情信息
   */
  @PostMapping("/chapter/detail/{chapterId}")
  public R<?> chapterDetail(@PathVariable Long chapterId) {
    return R.ok(historyBookChapterService.getChapterDetail(chapterId));
  }
}
