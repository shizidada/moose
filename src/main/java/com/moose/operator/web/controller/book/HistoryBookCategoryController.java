package com.moose.operator.web.controller.book;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.SearchParam;
import com.moose.operator.web.service.HistoryBookCategoryService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author taohua
 */

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/historybook")
public class HistoryBookCategoryController {

  @Resource
  private HistoryBookCategoryService historyBookCategoryService;

  @PostMapping("/category/list")
  public R<?> categoryList(@Valid SearchParam searchParam, BindingResult result) {
    return R.ok(historyBookCategoryService.getCategoryList(searchParam));
  }
}
