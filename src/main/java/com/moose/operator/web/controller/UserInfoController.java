package com.moose.operator.web.controller;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.UserInfoParam;
import com.moose.operator.web.service.AccountService;
import com.moose.operator.web.service.UserInfoService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author taohua 用户信息
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v1/user")
public class UserInfoController {

  @Resource
  private AccountService accountService;

  @Resource
  private UserInfoService userInfoService;

  @PostMapping(value = "/info")
  public R<?> info() {
    return R.ok(userInfoService.getCurrentUserInfo());
  }

  @PostMapping(value = "/update")
  public R<?> update(@Valid UserInfoParam userInfoParam, BindingResult result) {
    return R.ok(userInfoService.updateUserInfo(userInfoParam));
  }

  @PostMapping(value = "/resetPhone")
  public R<?> resetPhone(String phone, String smsCode) {
    return R.ok(userInfoService.resetPhone(phone, smsCode));
  }
}
