package com.moose.operator.web.controller;

import com.moose.operator.model.api.R;
import com.moose.operator.web.service.BannerService;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-20 20:07:20:07
 * @see com.moose.operator.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v1/banner")
public class BannerController {

  @Resource
  private BannerService bannerService;

  /**
   * get poetry main banner pic
   */
  @PostMapping(value = "/main")
  public R<?> mainBanner() {
    return R.ok(bannerService.getMainBannerInfoList());
  }
}
