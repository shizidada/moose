package com.moose.operator.web.controller;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.DynamicRecordParam;
import com.moose.operator.model.param.SearchParam;
import com.moose.operator.web.service.DynamicRecordService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author taohua
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v1/dynamic")
public class DynamicController {

  @Resource
  private DynamicRecordService dynamicRecordService;

  @PostMapping("/create")
  public R<?> create(@Valid DynamicRecordParam dynamicRecordParam, BindingResult result) {
    return R.ok(dynamicRecordService.saveDynamicRecord(dynamicRecordParam));
  }

  @GetMapping("/recommend/list")
  public R<?> recommendList(@Valid SearchParam searchParam, BindingResult result) {
    return R.ok(dynamicRecordService.getRecommendDynamicRecord(searchParam));
  }

  @GetMapping("/my/list")
  public R<?> my(@Valid SearchParam searchParam, BindingResult result) {
    return R.ok(dynamicRecordService.getMyDynamicRecord(searchParam));
  }

  @GetMapping("/detail/{dynamicId}")
  public R<?> detailDynamic(@PathVariable("dynamicId") String dynamicId) {
    return R.ok(dynamicRecordService.getDetailDynamicRecord(dynamicId));
  }
}
