package com.moose.operator.web.controller;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.SmsCodeParam;
import com.moose.operator.web.service.SmsCodeSenderService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-06-16 14:32:14:32
 * @see com.moose.operator.web.controller
 */

@RestController
@RequestMapping(value = "/api/v1/sms")
@Slf4j
public class SmsCodeController {
  @Resource
  private SmsCodeSenderService smsCodeSenderService;

  @PostMapping("/send")
  public R<?> sendSmsCode(@Valid SmsCodeParam smsCodeParam, BindingResult result) {
    return R.ok(smsCodeSenderService.sendSmsCode(smsCodeParam));
  }
}
