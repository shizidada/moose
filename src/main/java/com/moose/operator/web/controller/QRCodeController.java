package com.moose.operator.web.controller;

import com.moose.operator.model.api.R;
import com.moose.operator.web.service.QRCodeService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-03-27 21:23:21:23
 * @see com.moose.operator.web.controller
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/qrcode")
public class QRCodeController {

  @Resource
  private QRCodeService qrCodeService;

  /**
   * action: getqrcode random: 1616896702830
   * <p>
   * action: ask token: lang: zh_CN f: json ajax: 1
   */
  @GetMapping("get")
  public R<?> getQRCode(HttpServletResponse response, HttpServletRequest request) {
    return R.ok(qrCodeService.getQRCode(response, request));
  }

  @GetMapping("ask")
  public R<?> askQRCode(HttpServletRequest request) {
    return R.ok(qrCodeService.askQRCodeStatus(request));
  }

  @GetMapping("scanlogin")
  public R<?> scanLogin(HttpServletRequest request) {
    qrCodeService.scanLogin(request);
    return R.ok();
  }
}
