package com.moose.operator.web.controller;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.AuthTokenParam;
import com.moose.operator.model.param.LoginInfoParam;
import com.moose.operator.model.param.RegisterInfoParam;
import com.moose.operator.web.service.AccountService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author taohua
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v1/account")
public class AccountController {

  @Resource
  private AccountService accountService;

  /**
   * spring security oauth2.0 to register
   */
  @PostMapping(value = "/register")
  public R<?> register(
      HttpServletRequest request,
      @Valid RegisterInfoParam registerInfoParam, BindingResult result
  ) {
    return R.ok(accountService.register(request, registerInfoParam));
  }

  /**
   * spring security oauth2.0 to login
   */
  @PostMapping(value = "/login")
  public R<?> login(@Valid LoginInfoParam loginInfoParam, BindingResult result) {
    return R.ok(accountService.login(loginInfoParam));
  }

  /**
   * spring security oauth2.0 to logout
   */
  @PostMapping(value = "/logout")
  public R<?> logout(@Valid AuthTokenParam tokenParam, BindingResult result) {
    accountService.logout(tokenParam.getAccessToken());
    return R.ok();
  }

  @GetMapping(value = "/status")
  public R<?> status() {
    return R.ok(accountService.isLogin());
  }
}
