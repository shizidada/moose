package com.moose.operator.web.controller;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.AuthTokenParam;
import com.moose.operator.web.service.AccountService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author taohua
 */

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/token")
public class TokenController {

  @Resource
  private AccountService accountService;

  @PostMapping(value = "/refresh")
  public R<?> refreshToken(@Valid AuthTokenParam authTokenParam, BindingResult result) {
    return R.ok(accountService.refreshToken(authTokenParam.getAccessToken()));
  }
}
