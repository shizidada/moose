package com.moose.operator.web.controller.book;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.book.HistoryBookParam;
import com.moose.operator.web.service.HistoryBookService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author taohua
 */

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/historybook")
public class HistoryBookController {

  @Resource
  private HistoryBookService historyBookService;

  @PostMapping("/item/list")
  public R<?> itemList(@Valid HistoryBookParam searchParam, BindingResult result) {
    return R.ok(historyBookService.getHistoryBook(searchParam));
  }
}
