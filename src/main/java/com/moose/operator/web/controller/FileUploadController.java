package com.moose.operator.web.controller;

import com.moose.operator.model.api.R;
import com.moose.operator.web.service.FileUploadService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author taohua
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v1/file")
public class FileUploadController {

  @Resource
  private FileUploadService fileUploadService;

  @PostMapping(value = "/upload")
  public R<?> uploadFile(@RequestParam("files") MultipartFile[] files) {
    return R.ok(fileUploadService.uploadFile(files));
  }

  @PostMapping(value = "/signature")
  public R<?> signature() {
    return R.ok(fileUploadService.crateSignature());
  }

  /**
   * TODO：需要部署到外网环境 Ali OSS 才可以回调
   *
   * @param request
   * @param ossCallbackBody
   * @return
   */
  @PostMapping(value = "/callback")
  public R<?> callback(HttpServletRequest request, String ossCallbackBody) {
    return R.ok();
  }
}
