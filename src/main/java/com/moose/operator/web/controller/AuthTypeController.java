package com.moose.operator.web.controller;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.AuthTypeParam;
import com.moose.operator.web.service.AuthService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author taohua
 */

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/auth")
public class AuthTypeController {

  @Resource
  private AuthService authService;

  @PostMapping(value = "/getThirdUrl")
  public R<?> callbackLogin(@Valid AuthTypeParam authTypeParam, BindingResult bindingResult) {
    return R.ok(authService.genThirdPartUrl(authTypeParam.getAuthType()));
  }
}
