package com.moose.operator.web.controller.poetry;

import com.moose.operator.model.api.R;
import com.moose.operator.web.service.PoetryAuthorDetailInfoService;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-31 20:46:20:46
 * @see com.moose.operator.web.controller
 */

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/poetry")
public class PoetryAuthorDetailInfoController {

  @Resource
  private PoetryAuthorDetailInfoService poetryAuthorDetailInfoService;

  @PostMapping("/author/detail/{authorId}")
  public R<Object> authorDetailInfo(@PathVariable Integer authorId) {
    return R.ok(poetryAuthorDetailInfoService.detailPoetryInfo(authorId));
  }
}
