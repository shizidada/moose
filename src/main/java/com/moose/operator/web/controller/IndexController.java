package com.moose.operator.web.controller;

import com.moose.operator.annotation.Limiter;
import com.moose.operator.common.SnowflakeIdWorker;
import com.moose.operator.model.api.R;
import java.time.LocalDate;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Description
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2019 2019/10/27 15:06
 * @see com.moose.operator.web.controller
 */
@Slf4j
@RestController
public class IndexController {
  @Resource
  private SnowflakeIdWorker snowflakeIdWorker;

  @Resource
  private RedissonClient redissonClient;

  @GetMapping("/")
  public String index() {
    return "Moose Operator Service " + LocalDate.now();
  }

  @Limiter(limitType = Limiter.LimitType.IP)
  @GetMapping("/api/v1/test/limit")
  public R<?> test() {
    return R.ok();
  }

  @GetMapping("/api/v1/id/snowflake")
  public R<?> getId() {
    return R.ok(snowflakeIdWorker.nextId());
  }

  @GetMapping("/api/v1/test/redisson")
  public R<?> redisson() {
    RLock lock = redissonClient.getLock("TEST_REDISSON_KEY");
    try {
      lock.lock();
      Thread.sleep(2000);
    } catch (Exception e) {
      log.error(e.getMessage());
    } finally {
      lock.unlock();
    }
    return R.ok();
  }
}
