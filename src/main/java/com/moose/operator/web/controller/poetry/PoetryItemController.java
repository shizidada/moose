package com.moose.operator.web.controller.poetry;

import com.moose.operator.model.api.R;
import com.moose.operator.model.param.SearchParam;
import com.moose.operator.model.param.poetry.PoetrySearchParam;
import com.moose.operator.web.service.PoetryItemService;
import javax.annotation.Resource;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Description: 获取诗词列表信息
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-13 21:44:21:44
 * @see com.moose.operator.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v1/poetry")
public class PoetryItemController {

  @Resource
  private PoetryItemService poetryItemService;

  @PostMapping("/item/list")
  public R<?> poetryList(@Valid PoetrySearchParam searchParam, BindingResult result) {
    return R.ok(poetryItemService.listPoetry(searchParam));
  }

  @PostMapping("/ranking/list")
  public R<?> rankingList(@Valid SearchParam searchParam, BindingResult result) {
    return R.ok(poetryItemService.listPoetryRanking(searchParam));
  }
}
