package com.moose.operator.web.filter;

import com.moose.operator.constant.AnonymousUrlConstant;
import com.moose.operator.constant.CacheNameConstant;
import com.moose.operator.constant.CommonConstant;
import com.moose.operator.constant.SecurityConstant;
import com.moose.operator.exception.BusinessException;
import com.moose.operator.model.api.ResultCode;
import com.moose.operator.model.emun.SmsCodeEnum;
import com.moose.operator.util.RedisHelper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * @author taohua
 */
@Slf4j
public class MooseSmsVerifyLimitFilter extends OncePerRequestFilter {

  /**
   * 存放所有需要校验验证码的url
   */
  private final Map<String, String> urlMap = new HashMap<>(16);
  private final AuthenticationFailureHandler authenticationFailureHandler;
  private RedisHelper redisHelper;

  public MooseSmsVerifyLimitFilter(RedisHelper redisHelper,
      AuthenticationFailureHandler authenticationFailureHandler) {
    this.redisHelper = redisHelper;
    this.authenticationFailureHandler = authenticationFailureHandler;
  }

  @Override public void afterPropertiesSet() throws ServletException {
    super.afterPropertiesSet();

    addUrlToMap(AnonymousUrlConstant.SEND_SMS_CODE_URL,
        CommonConstant.DEFAULT_PARAMETER_NAME_PHONE);
  }

  protected void addUrlToMap(String urlString, String smsParam) {
    if (StringUtils.isNotBlank(urlString)) {
      String[] urls = StringUtils.splitByWholeSeparatorPreserveAllTokens(urlString, ",");
      for (String url : urls) {
        urlMap.put(url, smsParam);
      }
    }
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    if (ObjectUtils.isNotEmpty(urlMap.get(request.getRequestURI())) &&
        StringUtils.equalsIgnoreCase(request.getMethod(), CommonConstant.HTTP_METHOD_POST)) {
      try {
        validate(request);
      } catch (BusinessException e) {
        authenticationFailureHandler.onAuthenticationFailure(request, response, e);
        return;
      }
    }
    filterChain.doFilter(request, response);
  }

  /**
   * 短信发送校验
   * <p>
   * TODO: 短信提交校验
   *
   * @param request HttpServletRequest
   */
  private void validate(HttpServletRequest request) {
    String smsType = request.getParameter(CommonConstant.DEFAULT_PARAMETER_NAME_SMS_TYPE);
    if (!SmsCodeEnum.isExist(smsType)) {
      throw new BusinessException(ResultCode.SMS_TYPE_ERROR);
    }

    String phoneNumber = request.getParameter(CommonConstant.DEFAULT_PARAMETER_NAME_PHONE);

    // 计算发送次数
    String smsSendCountKey = CacheNameConstant.SMS_PHONE_KEY + phoneNumber;
    Integer sendCount = (Integer) redisHelper.getValue(smsSendCountKey);
    if (ObjectUtils.isEmpty(sendCount)) {
      redisHelper.cacheValue(smsSendCountKey, 1, SecurityConstant.SMS_TIME_OF_DAY, TimeUnit.SECONDS);
    } else {
      // 判断手机号时间范围内，累计发送次数
      if (sendCount >= SecurityConstant.MAX_COUNT_OF_DAY) {
        throw new BusinessException(ResultCode.SMS_CODE_COUNT);
      }
      redisHelper.increment(smsSendCountKey, 1);
      redisHelper.expire(smsSendCountKey, SecurityConstant.SMS_TIME_OF_DAY, TimeUnit.SECONDS);
    }
  }
}
