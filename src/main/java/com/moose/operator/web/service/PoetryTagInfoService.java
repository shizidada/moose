package com.moose.operator.web.service;

import com.moose.operator.common.PageInfo;
import com.moose.operator.model.entity.PoetryTagInfo;
import com.moose.operator.model.param.SearchParam;
import java.util.List;

/**
 * @author taohua
 */
public interface PoetryTagInfoService {

  /**
   * 获取 tag 列表 分页
   *
   * @param searchParam 查询参数
   * @return List<PoetryTagInfo>
   */
  PageInfo<PoetryTagInfo> listPoetryTag(SearchParam searchParam);

  /**
   * 获取 tag 列表 Top10
   *
   * @return List<PoetryTagInfo>
   */
  PageInfo<PoetryTagInfo> tagListTop10();
}
