package com.moose.operator.web.service.impl.poetry;

import com.moose.operator.common.QueryWrapper;
import com.moose.operator.model.entity.PoetryAuthorDetailInfo;
import com.moose.operator.web.service.PoetryAuthorDetailInfoService;
import javax.annotation.Resource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-31 20:48:20:48
 * @see com.moose.operator.web.service.impl
 */
@Service
public class PoetryAuthorDetailInfoServiceImpl implements PoetryAuthorDetailInfoService {
  @Resource
  private MongoTemplate mongoTemplate;

  @Override public PoetryAuthorDetailInfo detailPoetryInfo(Integer authorId) {
    //"allusion", "life", "achievement", "evaluation",
    Query query = QueryWrapper.buildQuery();
    query.addCriteria(Criteria.where("author_id").is(authorId));
    query.fields()
        .include("author_id")
        .include("allusion")
        .include("life")
        .include("achievement")
        .include("evaluation");
    return mongoTemplate.findOne(query, PoetryAuthorDetailInfo.class);
  }
}
