package com.moose.operator.web.service.impl;

import cn.hutool.core.util.IdUtil;
import com.moose.operator.constant.CacheNameConstant;
import com.moose.operator.constant.CommonConstant;
import com.moose.operator.constant.SecurityConstant;
import com.moose.operator.exception.BusinessException;
import com.moose.operator.model.api.ResultCode;
import com.moose.operator.web.service.QRCodeService;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-03-27 22:41:22:41
 * @see com.moose.operator.web.service.impl
 */

@Service
@Slf4j
public class QRCodeServiceImpl implements QRCodeService {

  @Resource
  private Redisson redisson;

  /**
   * 获取二维码
   */
  @Override public String getQRCode(HttpServletResponse response, HttpServletRequest request) {
    // https://gitee.com/shizidada/moose-resource/raw/master/blog/qrcode_430.jpg
    String mTicket = IdUtil.simpleUUID();
    String qrCodeUrl =
        String.format("http://192.168.1.100:7000/api/v1/qrcode/scanlogin?m_ticket=%s", mTicket);
    RMap<Object, Object> ticketMap = redisson.getMap(CacheNameConstant.SCAN_TICKETS_KEY);
    // redis map 保存 key
    ticketMap.fastPut(mTicket, 0);
    // 设置过期时间 60 秒
    ticketMap.expire(SecurityConstant.SCAN_TICKET_VALIDITY, TimeUnit.SECONDS);

    Cookie cookie = new Cookie(CommonConstant.QR_TICKET, mTicket);
    cookie.setHttpOnly(true);
    cookie.setMaxAge(SecurityConstant.SCAN_TICKET_VALIDITY);
    response.addCookie(cookie);

    return qrCodeUrl;
  }

  @Override public Integer askQRCodeStatus(HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    if (ObjectUtils.isEmpty(cookies)) {
      throw new BusinessException(ResultCode.QR_CODE_TICKET_EMPTY);
    }

    String mTicket = null;
    for (Cookie cookie : cookies) {
      if (CommonConstant.QR_TICKET.equals(cookie.getName())) {
        mTicket = cookie.getValue();
      }
    }

    if (StringUtils.isEmpty(mTicket)) {
      throw new BusinessException(ResultCode.QR_CODE_TICKET_EMPTY);
    }

    RMap<Object, Object> ticketMap = redisson.getMap(CacheNameConstant.SCAN_TICKETS_KEY);
    Integer status = (Integer) ticketMap.get(mTicket);
    if (null != status && status == 1) {
      return 1;
    }
    return 0;
  }

  @Override public void scanLogin(HttpServletRequest request) {
    String mTicket = request.getParameter(CommonConstant.QR_TICKET);
    RMap<Object, Object> ticketMap = redisson.getMap(CacheNameConstant.SCAN_TICKETS_KEY);
    if (!ObjectUtils.isEmpty(ticketMap.get(mTicket))) {
      ticketMap.put(mTicket, 1);
    }
  }
}
