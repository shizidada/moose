package com.moose.operator.web.service.impl.attachment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moose.operator.exception.BusinessException;
import com.moose.operator.model.api.ResultCode;
import com.moose.operator.model.domain.AttachmentRecordDO;
import com.moose.operator.model.domain.DynamicRecordDO;
import com.moose.operator.model.dto.AttachmentRecordDTO;
import com.moose.operator.model.dto.FileUploadInfoDTO;
import com.moose.operator.util.OSSClientUtils;
import com.moose.operator.web.service.AttachmentRecordService;
import com.moose.operator.web.service.FileUploadService;
import com.moose.operator.web.service.UserInfoService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author taohua
 */
@Service
@Slf4j
public class FileUploadServiceImpl implements FileUploadService {

  @Resource
  private UserInfoService userInfoService;

  @Resource
  private AttachmentRecordService attachmentRecordService;

  @Resource
  private ObjectMapper objectMapper;

  @Resource
  private OSSClientUtils ossClientUtils;

  @Override public List<FileUploadInfoDTO> uploadFile(MultipartFile[] files) {
    if (ObjectUtils.isEmpty(files)) {
      throw new BusinessException(ResultCode.FILE_NOT_EMPTY);
    }
    List<FileUploadInfoDTO> fileUploadDTOList = new ArrayList<>();

    for (MultipartFile file : files) {
      String originalFilename = file.getOriginalFilename();
      if (StringUtils.isEmpty(originalFilename)) {
        throw new BusinessException(ResultCode.FILE_LEGITIMATE_ERROR);
      }

      // OSSConstants.USER_AVATAR_BUCKET_NAME_KEY,
      FileUploadInfoDTO fileUploadInfoDTO = ossClientUtils.uploadFile(file);
      fileUploadDTOList.add(fileUploadInfoDTO);
      log.info("文件上传成功 : {} ", fileUploadInfoDTO);
    }

    if (fileUploadDTOList.size() > 0) {
      attachmentRecordService.batchSaveAttachmentRecord(fileUploadDTOList);
    }
    return fileUploadDTOList;
  }

  @Override public Map<String, String> crateSignature() {
    return ossClientUtils.getSignature();
  }

  /**
   * convert DO to DTO
   *
   * @param dynamicRecordDO data object
   * @return dto
   */
  private AttachmentRecordDO convertDTOFromDO(DynamicRecordDO dynamicRecordDO) {
    return null;
  }
}
