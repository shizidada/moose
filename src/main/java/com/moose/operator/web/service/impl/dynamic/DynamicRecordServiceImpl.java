package com.moose.operator.web.service.impl.dynamic;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.moose.operator.common.SnowflakeIdWorker;
import com.moose.operator.constant.CacheNameConstant;
import com.moose.operator.constant.CommonConstant;
import com.moose.operator.exception.BusinessException;
import com.moose.operator.mapper.AttachmentRecordMapper;
import com.moose.operator.mapper.DynamicRecordAttachmentRelationMapper;
import com.moose.operator.mapper.DynamicRecordMapper;
import com.moose.operator.model.api.ResultCode;
import com.moose.operator.model.domain.AttachmentRecordDO;
import com.moose.operator.model.domain.DynamicRecordAttachmentRelationDO;
import com.moose.operator.model.domain.DynamicRecordDO;
import com.moose.operator.model.domain.UserInfoDO;
import com.moose.operator.model.dto.DynamicRecordDTO;
import com.moose.operator.model.dto.FileUploadInfoDTO;
import com.moose.operator.model.dto.UserBaseInfoDTO;
import com.moose.operator.model.dto.UserInfoDTO;
import com.moose.operator.model.param.AttachmentParam;
import com.moose.operator.model.param.DynamicRecordParam;
import com.moose.operator.model.param.SearchParam;
import com.moose.operator.util.ListBeanUtils;
import com.moose.operator.util.PageInfoUtils;
import com.moose.operator.util.RedisHelper;
import com.moose.operator.web.service.AccountService;
import com.moose.operator.web.service.AttachmentRecordService;
import com.moose.operator.web.service.DynamicRecordService;
import com.moose.operator.web.service.UserInfoService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author taohua
 */
@Service
public class DynamicRecordServiceImpl implements DynamicRecordService {

  @Resource
  private UserInfoService userInfoService;

  @Resource
  private AccountService accountService;

  @Resource
  private RedisHelper redisHelper;

  @Resource
  private AttachmentRecordService attachmentRecordService;

  @Resource
  private SnowflakeIdWorker snowflakeIdWorker;

  @Resource
  private DynamicRecordMapper dynamicRecordMapper;

  @Resource
  private DynamicRecordAttachmentRelationMapper attachmentRelationMapper;

  @Resource
  private AttachmentRecordMapper attachmentRecordMapper;

  @Transactional(rollbackFor = Exception.class)
  @Override
  public boolean saveDynamicRecord(DynamicRecordParam dynamicRecordParam) {
    List<AttachmentParam> attachments = dynamicRecordParam.getAttachments();
    if (ObjectUtils.isNotEmpty(attachments)
        && attachments.size() > CommonConstant.UPLOAD_ATTACHMENT_SIZE) {
      throw new BusinessException(ResultCode.UPLOAD_ATTACHMENT_SIZE_ERROR);
    }

    UserInfoDTO userInfo = userInfoService.getCurrentUserInfo();
    Long userId = userInfo.getUserId();

    if (ObjectUtils.isNotEmpty(attachments) && attachments.size() > 0) {
      this.checkAttachment(attachments, userId);
    }

    DynamicRecordDTO dynamicRecordDTO = new DynamicRecordDTO();
    BeanUtils.copyProperties(dynamicRecordParam, dynamicRecordDTO);

    DynamicRecordDO dynamicRecordDO = new DynamicRecordDO();
    BeanUtils.copyProperties(dynamicRecordDTO, dynamicRecordDO);
    dynamicRecordDO.setUserId(userId);
    dynamicRecordDO.setDrId(snowflakeIdWorker.nextId());
    dynamicRecordMapper.insertDynamicRecord(dynamicRecordDO);

    if (ObjectUtils.isNotEmpty(attachments)) {
      List<FileUploadInfoDTO> attachDTOList =
          ListBeanUtils.copyListProperties(attachments, FileUploadInfoDTO::new);
      dynamicRecordDTO.setAttachments(attachDTOList);

      List<DynamicRecordAttachmentRelationDO> attachRelaDOList =
          new ArrayList<>(attachDTOList.size());

      // dynamicRecord rela attachmentRecord
      DynamicRecordAttachmentRelationDO attachRelaDO = new DynamicRecordAttachmentRelationDO();
      attachRelaDO.setDrId(dynamicRecordDO.getDrId());
      attachRelaDO.setAttachId(snowflakeIdWorker.nextId());
      attachRelaDO.setDraId(snowflakeIdWorker.nextId());
      attachRelaDOList.add(attachRelaDO);

      if (attachRelaDOList.size() > 0) {
        attachmentRelationMapper.batchInsertDynamicRecordRelation(attachRelaDOList);
      }
    }
    return Boolean.TRUE;
  }

  @Override
  public Map<String, Object> getMyDynamicRecord(SearchParam searchParam) {
    PageHelper.startPage(searchParam);
    // 根据当前登录用户查询动态记录
    UserInfoDTO userInfo = userInfoService.getCurrentUserInfo();
    List<DynamicRecordDO> dynamicRecordList =
        dynamicRecordMapper.selectByUserId(userInfo.getUserId());

    PageInfo<DynamicRecordDO> pageInfo = new PageInfo<>(dynamicRecordList);
    List<DynamicRecordDTO> dynamicRecordDTOList =
        pageInfo.getList().stream().map(this::convertDTOFromDO).collect(Collectors.toList());
    Map<String, Object> result = PageInfoUtils.getPageInfo(pageInfo);
    result.put("lists", dynamicRecordDTOList);
    return result;
  }

  // 获取推荐
  @Override public Map<String, Object> getRecommendDynamicRecord(SearchParam searchParam) {
    PageHelper.startPage(searchParam);
    List<DynamicRecordDO> dynamicRecordList = dynamicRecordMapper.selectBaseDynamicRecordInfo();
    PageInfo<DynamicRecordDO> pageInfo = new PageInfo<>(dynamicRecordList);
    List<DynamicRecordDO> dynamicRecordDOList = pageInfo.getList();
    List<DynamicRecordDTO> dynamicRecordDTOList =
        dynamicRecordDOList.stream().map(this::convertDTOFromDO).collect(Collectors.toList());
    Map<String, Object> result = PageInfoUtils.getPageInfo(pageInfo);
    result.put("lists", dynamicRecordDTOList);
    return result;
  }

  // 获取动态详情
  @Override public DynamicRecordDTO getDetailDynamicRecord(String dynamicId) {
    DynamicRecordDO dynamicRecordDO = dynamicRecordMapper.selectDynamicRecordByDynamicId(dynamicId);
    return this.convertDTOFromDO(dynamicRecordDO);
  }

  // 检查附件是否上传过
  private void checkAttachment(List<AttachmentParam> attachmentIds, Long userId) {
    for (AttachmentParam attachmentParam : attachmentIds) {
      String tag = attachmentParam.getTag();
      AttachmentRecordDO attachmentRecordDO = attachmentRecordMapper.selectByUserIdAndEtag(userId, tag);
      if (ObjectUtils.isEmpty(attachmentRecordDO)) {
        throw new BusinessException(ResultCode.UPLOAD_ATTACHMENT_RECORD_NOT_EXIST);
      }
    }
  }

  /**
   * convert DO to DTO
   *
   * @param dynamicRecordDO data object
   * @return dto
   */
  private DynamicRecordDTO convertDTOFromDO(DynamicRecordDO dynamicRecordDO) {
    if (ObjectUtils.isEmpty(dynamicRecordDO)) {
      return null;
    }

    Long drId = dynamicRecordDO.getDrId();

    List<DynamicRecordAttachmentRelationDO> attachmentRelationDOList =
        attachmentRelationMapper.selectByDynamicRecordId(drId);

    List<FileUploadInfoDTO> uploadDTOList =
        attachmentRelationDOList.stream().map((attachmentRelationDO) -> {
          Long attachId = attachmentRelationDO.getAttachId();
          AttachmentRecordDO attachmentRecordDO =
              attachmentRecordMapper.selectFileUrlByAttachId(attachId);
          if (ObjectUtils.isEmpty(attachmentRecordDO)) {
            return null;
          }
          FileUploadInfoDTO fileUploadInfoDTO = new FileUploadInfoDTO();
          fileUploadInfoDTO.setFileUrl(attachmentRecordDO.getFileUrl());
          return fileUploadInfoDTO;
        }).filter(ObjectUtils::isNotEmpty).collect(Collectors.toList());

    DynamicRecordDTO dynamicRecordDTO = new DynamicRecordDTO();
    BeanUtils.copyProperties(dynamicRecordDO, dynamicRecordDTO);
    dynamicRecordDTO.setAttachments(uploadDTOList);

    UserInfoDO author = dynamicRecordDO.getAuthor();
    if (ObjectUtils.isNotEmpty(author)) {
      UserBaseInfoDTO userBaseInfo = new UserBaseInfoDTO();
      BeanUtils.copyProperties(author, userBaseInfo);
      dynamicRecordDTO.setAuthor(userBaseInfo);
    }

    // setting liked
    if (accountService.isLogin()) {
      UserInfoDTO userInfo = userInfoService.getCurrentUserInfo();
      String likedKey = String.format(CacheNameConstant.USER_LIKED_KEY, userInfo.getUserId());
      Integer liked = (Integer) redisHelper.hashGet(likedKey, drId.toString());
      dynamicRecordDTO.setLike((null == liked || liked == 0) ? 0 : 1);
    } else {
      dynamicRecordDTO.setLike(0);
    }
    return dynamicRecordDTO;
  }
}
