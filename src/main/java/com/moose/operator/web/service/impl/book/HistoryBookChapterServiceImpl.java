package com.moose.operator.web.service.impl.book;

import com.moose.operator.common.PageInfo;
import com.moose.operator.common.QueryWrapper;
import com.moose.operator.model.entity.HistoryBookChapter;
import com.moose.operator.model.entity.HistoryBookChapterDetail;
import com.moose.operator.model.param.book.HistoryBookChapterParam;
import com.moose.operator.web.service.HistoryBookChapterService;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author taohua
 */
@Service
public class HistoryBookChapterServiceImpl implements HistoryBookChapterService {

  @Resource
  private MongoTemplate mongoTemplate;

  @Override
  public PageInfo<HistoryBookChapter> getChapterList(HistoryBookChapterParam searchParam) {

    Integer pageNum = searchParam.getPageNum();
    Integer pageSize = searchParam.getPageSize();
    Long bookId = searchParam.getBookId();
    Aggregation aggregation = Aggregation.newAggregation(
        Aggregation.project(
            "is_category_title", "name", "sort", "book_id", "chapter_id"),

        Aggregation.match(Criteria.where("book_id").is(bookId)),

        QueryWrapper.buildSkipOperation(pageNum, pageSize),
        QueryWrapper.buildLimitOperation(pageSize));

    AggregationResults<Document> aggregate =
        mongoTemplate.aggregate(aggregation, "history_book_chapter", Document.class);

    List<Document> mappedResults = aggregate.getMappedResults();

    List<HistoryBookChapter> historyBookCategoryList = mappedResults.stream().map(document -> {
      HistoryBookChapter historyBookChapter = new HistoryBookChapter();
      historyBookChapter.setIsCategoryTitle(document.getInteger("is_category_title"));
      historyBookChapter.setName(document.getString("name"));
      historyBookChapter.setSort(document.getInteger("sort"));
      historyBookChapter.setBookId(document.getLong("book_id"));
      historyBookChapter.setChapterId(document.getLong("chapter_id"));
      return historyBookChapter;
    }).collect(Collectors.toList());

    Query query = QueryWrapper.buildQuery();
    query.addCriteria(Criteria.where("book_id").is(bookId));
    long totalSize = mongoTemplate.count(query, HistoryBookChapter.class);

    PageInfo<HistoryBookChapter> pageInfo = new PageInfo<>();
    pageInfo.setList(historyBookCategoryList);
    pageInfo.setPageSize(pageSize);
    pageInfo.setTotalSize(totalSize);
    pageInfo.setPageNum(pageNum);
    return pageInfo;
  }

  @Override public HistoryBookChapterDetail getChapterDetail(Long chapterId) {
    if (null == chapterId) {
      return null;
    }
    Query query = QueryWrapper.buildQuery();
    Criteria criteria = Criteria.where("chapter_id").is(chapterId);
    query.addCriteria(criteria);
    return mongoTemplate.findOne(query, HistoryBookChapterDetail.class);
  }
}
