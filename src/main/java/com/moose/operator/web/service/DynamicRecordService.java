package com.moose.operator.web.service;

import com.moose.operator.model.dto.DynamicRecordDTO;
import com.moose.operator.model.param.DynamicRecordParam;
import com.moose.operator.model.param.SearchParam;
import java.util.Map;

/**
 * @author taohua
 */
public interface DynamicRecordService {

  /**
   * save dynamic record
   *
   * @param dynamicRecordParam record
   * @return success
   */
  boolean saveDynamicRecord(DynamicRecordParam dynamicRecordParam);

  /**
   * get my dynamic record
   *
   * @param searchParam search param
   * @return list of dynamic record me
   */
  Map<String, Object> getMyDynamicRecord(SearchParam searchParam);

  /**
   * get recommend dynamic record by step
   *
   * @param searchParam search param
   * @return list of recommend dynamic record
   */
  Map<String, Object> getRecommendDynamicRecord(SearchParam searchParam);

  /**
   * get detail dynamic
   *
   * @param dynamicId dynamic Id
   * @return DynamicRecordDTO
   */
  DynamicRecordDTO getDetailDynamicRecord(String dynamicId);
}
