package com.moose.operator.web.service;

import com.moose.operator.model.dto.FileUploadInfoDTO;
import java.util.List;

/**
 * @author taohua
 */
public interface AttachmentRecordService {

  /**
   * batch save file upload info
   *
   * @param attachmentUploadDTOList upload info
   */
  void batchSaveAttachmentRecord(List<FileUploadInfoDTO> attachmentUploadDTOList);
}
