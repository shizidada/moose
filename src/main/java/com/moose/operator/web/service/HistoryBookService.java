package com.moose.operator.web.service;

import com.moose.operator.common.PageInfo;
import com.moose.operator.model.entity.HistoryBook;
import com.moose.operator.model.param.book.HistoryBookParam;

/**
 * @author taohua
 */
public interface HistoryBookService {

  /**
   * 获取历史书籍分类 对应书籍
   *
   * @param searchParam 搜索条件
   * @return PageInfo<HistoryBook>
   */
  PageInfo<HistoryBook> getHistoryBook(HistoryBookParam searchParam);
}
