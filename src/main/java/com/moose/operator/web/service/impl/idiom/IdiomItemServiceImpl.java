package com.moose.operator.web.service.impl.idiom;

import com.moose.operator.common.PageInfo;
import com.moose.operator.common.QueryWrapper;
import com.moose.operator.model.entity.IdiomItem;
import com.moose.operator.model.entity.IdiomStructure;
import com.moose.operator.model.entity.IdiomType;
import com.moose.operator.model.param.IdiomSearchParam;
import com.moose.operator.web.service.IdiomItemService;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author taohua
 */
@Service
public class IdiomItemServiceImpl implements IdiomItemService {

  @Resource
  private MongoTemplate mongoTemplate;

  @Override public PageInfo<IdiomItem> idiomItemList(IdiomSearchParam searchParam) {
    Integer pageNum = searchParam.getPageNum();
    Integer pageSize = searchParam.getPageSize();
    Aggregation aggregation = Aggregation.newAggregation(
        Aggregation.project("id", "chengyu", "f_pinyin", "xingshi", "duyin",
            "jieshi", "chuchu", "zaoju", "yongfa", "secai", "jiegou", "mingzhu", "mingren"),
        QueryWrapper.buildSkipOperation(pageNum, pageSize),
        QueryWrapper.buildLimitOperation(pageSize));

    AggregationResults<Document> aggregate =
        mongoTemplate.aggregate(aggregation, "idiom_item", Document.class);

    List<Document> mappedResults = aggregate.getMappedResults();

    List<IdiomItem> idiomItems = mappedResults.stream().map(document -> {
      IdiomItem idiomItem = new IdiomItem();
      idiomItem.setId(document.getInteger("id"));
      idiomItem.setChengyu(document.getString("chengyu"));
      idiomItem.setFPinyin(document.getString("f_pinyin"));
      idiomItem.setXingshi(document.getString("xingshi"));
      idiomItem.setDuyin(document.getString("duyin"));
      idiomItem.setJieshi(document.getString("jieshi"));

      idiomItem.setChuchu(document.getString("chuchu"));
      idiomItem.setZaoju(document.getString("zaoju"));
      idiomItem.setYongfa(document.getString("yongfa"));
      idiomItem.setSecai(document.getString("secai"));
      idiomItem.setJiegou(document.getString("jiegou"));
      idiomItem.setMingzhu(document.getString("mingzhu"));
      idiomItem.setMingren(document.getString("mingren"));


      return idiomItem;
    }).collect(Collectors.toList());

    // 计算总数
    Query query = QueryWrapper.buildQuery();
    long totalSize = mongoTemplate.count(query, IdiomItem.class);
    PageInfo<IdiomItem> pageInfo = new PageInfo<>();
    pageInfo.setList(idiomItems);
    pageInfo.setPageSize(pageSize);
    pageInfo.setTotalSize(totalSize);
    pageInfo.setPageNum(pageNum);
    return pageInfo;
  }

  @Override public PageInfo<IdiomStructure> idiomStructureList(IdiomSearchParam searchParam) {
    Integer pageNum = searchParam.getPageNum();
    Integer pageSize = searchParam.getPageSize();
    Aggregation aggregation = Aggregation.newAggregation(
        Aggregation.project("py_name", "cn_name"),
        QueryWrapper.buildSkipOperation(pageNum, pageSize),
        QueryWrapper.buildLimitOperation(pageSize));

    AggregationResults<Document> aggregate =
        mongoTemplate.aggregate(aggregation, "idiom_structure", Document.class);

    List<Document> mappedResults = aggregate.getMappedResults();

    List<IdiomStructure> idiomStructures = mappedResults.stream().map(document -> {
      IdiomStructure idiomStructure = new IdiomStructure();
      idiomStructure.setCnName(document.getString("cn_name"));
      idiomStructure.setPyName(document.getString("py_name"));
      return idiomStructure;
    }).collect(Collectors.toList());

    // 计算总数
    Query query = QueryWrapper.buildQuery();
    long totalSize = mongoTemplate.count(query, IdiomStructure.class);
    PageInfo<IdiomStructure> pageInfo = new PageInfo<>();
    pageInfo.setList(idiomStructures);
    pageInfo.setPageSize(pageSize);
    pageInfo.setTotalSize(totalSize);
    pageInfo.setPageNum(pageNum);
    return pageInfo;
  }

  @Override public PageInfo<IdiomType> idiomTypeList(IdiomSearchParam searchParam) {
    Integer pageNum = searchParam.getPageNum();
    Integer pageSize = searchParam.getPageSize();
    Aggregation aggregation = Aggregation.newAggregation(
        Aggregation.project("py_name", "cn_name"),
        QueryWrapper.buildSkipOperation(pageNum, pageSize),
        QueryWrapper.buildLimitOperation(pageSize));

    AggregationResults<Document> aggregate =
        mongoTemplate.aggregate(aggregation, "idiom_type", Document.class);

    List<Document> mappedResults = aggregate.getMappedResults();

    List<IdiomType> idiomTypes = mappedResults.stream().map(document -> {
      IdiomType idiomType = new IdiomType();
      idiomType.setCnName(document.getString("cn_name"));
      idiomType.setPyName(document.getString("py_name"));
      return idiomType;
    }).collect(Collectors.toList());

    // 计算总数
    Query query = QueryWrapper.buildQuery();
    long totalSize = mongoTemplate.count(query, IdiomType.class);
    PageInfo<IdiomType> pageInfo = new PageInfo<>();
    pageInfo.setList(idiomTypes);
    pageInfo.setPageSize(pageSize);
    pageInfo.setTotalSize(totalSize);
    pageInfo.setPageNum(pageNum);
    return pageInfo;
  }
}
