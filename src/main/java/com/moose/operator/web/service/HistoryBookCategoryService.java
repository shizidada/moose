package com.moose.operator.web.service;

import com.moose.operator.common.PageInfo;
import com.moose.operator.model.entity.HistoryBookCategory;
import com.moose.operator.model.param.SearchParam;

/**
 * @author taohua
 */
public interface HistoryBookCategoryService {

  /**
   * 获取所有历史书籍分类
   *
   * @param searchParam 搜索条件
   * @return PageInfo<HistoryBookCategory>
   */
  PageInfo<HistoryBookCategory> getCategoryList(SearchParam searchParam);
}
