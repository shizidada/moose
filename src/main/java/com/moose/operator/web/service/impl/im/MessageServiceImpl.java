package com.moose.operator.web.service.impl.im;

import com.moose.operator.web.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-01-23 19:54:19:54
 * @see com.moose.operator.web.service.impl.im
 */

@Slf4j
@Service
public class MessageServiceImpl implements MessageService {

}
