package com.moose.operator.web.service;

import com.moose.operator.model.entity.PoetryAuthorDetailInfo;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-31 20:47:20:47
 * @see com.moose.operator.web.service
 */
public interface PoetryAuthorDetailInfoService {

  /**
   * 获取 诗人详细信息
   *
   * @param authorId 诗人 id
   * @return 诗人信息
   */
  PoetryAuthorDetailInfo detailPoetryInfo(Integer authorId);
}
