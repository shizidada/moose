package com.moose.operator.web.service;

import com.moose.operator.common.PageInfo;
import com.moose.operator.model.entity.IdiomItem;
import com.moose.operator.model.entity.IdiomStructure;
import com.moose.operator.model.entity.IdiomType;
import com.moose.operator.model.param.IdiomSearchParam;

/**
 * @author taohua
 */
public interface IdiomItemService {

  /**
   * 获取成语列表
   *
   * @param searchParam IdiomSearchParam
   * @return PageInfo<IdiomItem>
   */
  PageInfo<IdiomItem> idiomItemList(IdiomSearchParam searchParam);

  /**
   * 获取成语 结构 列表
   *
   * @param searchParam IdiomSearchParam
   * @return PageInfo<IdiomItem>
   */
  PageInfo<IdiomStructure> idiomStructureList(IdiomSearchParam searchParam);

  /**
   * 获取成语 类型列表
   *
   * @param searchParam IdiomSearchParam
   * @return PageInfo<IdiomItem>
   */
  PageInfo<IdiomType> idiomTypeList(IdiomSearchParam searchParam);
}
