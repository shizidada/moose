package com.moose.operator.web.service;

import com.moose.operator.model.dto.GithubUserInfoDTO;

/**
 * @author taohua
 */

public interface AuthService {

  /**
   * 生成对应跳转 链接
   *
   * @param type 授权类型
   * @return 授权跳转 url
   */
  String genThirdPartUrl(String type);

  /**
   * github 授权
   *
   * @param code 授权码
   * @return 授权信息
   */
  GithubUserInfoDTO authGithub(String code);
}
