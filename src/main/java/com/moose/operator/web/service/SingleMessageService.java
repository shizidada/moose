package com.moose.operator.web.service;

import com.moose.operator.model.template.MessageTemplate;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-02-05 09:52:09:52
 * @see com.moose.operator.web.service
 */
public interface SingleMessageService {

  /**
   * 保存单聊信息
   *
   * @param template 消息模版
   */
  void saveMessage(MessageTemplate template);
}
