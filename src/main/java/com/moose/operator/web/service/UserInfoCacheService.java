package com.moose.operator.web.service;

import com.moose.operator.model.dto.UserInfoDTO;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-10-17 20:32:20:32
 * @see com.moose.operator.web.service
 */
public interface UserInfoCacheService {

  /**
   * save user info redis cache
   *
   * @param userInfoDTO user info
   */
  void saveUserInfoToCacheByAccountId(UserInfoDTO userInfoDTO);

  /**
   * get user info form redis cache by accountId
   *
   * @return user info
   */
  UserInfoDTO getUserInfoFromCacheByAccountId();

  /**
   * remove redis cache user info
   */
  void removeCacheUserInfoByAccountId();

}
