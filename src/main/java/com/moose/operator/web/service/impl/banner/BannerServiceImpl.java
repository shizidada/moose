package com.moose.operator.web.service.impl.banner;

import com.moose.operator.model.dto.BannerInfoDTO;
import com.moose.operator.web.service.BannerService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-20 20:10:20:10
 * @see com.moose.operator.web.service.impl
 */

@Service
public class BannerServiceImpl implements BannerService {

  @Override public Map<String, Object> getMainBannerInfoList() {

    // TODO: mock
    BannerInfoDTO bannerInfo = new BannerInfoDTO();
    bannerInfo.setImagePath("https://moose-plus.oss-cn-shenzhen.aliyuncs.com/2021-01-09/banner-1.png");

    BannerInfoDTO bannerInfo2 = new BannerInfoDTO();
    bannerInfo2.setImagePath("https://moose-plus.oss-cn-shenzhen.aliyuncs.com/2021-01-09/banner-2.png");

    BannerInfoDTO bannerInfo3 = new BannerInfoDTO();
    bannerInfo3.setImagePath("https://moose-plus.oss-cn-shenzhen.aliyuncs.com/2021-01-09/banner-3.png");

    List<BannerInfoDTO> bannerInfoDTOList = new ArrayList<>();
    bannerInfoDTOList.add(bannerInfo);
    bannerInfoDTOList.add(bannerInfo2);
    bannerInfoDTOList.add(bannerInfo3);

    Map<String, Object> result = new HashMap<>(16);
    result.put("list", bannerInfoDTOList);
    return result;
  }
}
