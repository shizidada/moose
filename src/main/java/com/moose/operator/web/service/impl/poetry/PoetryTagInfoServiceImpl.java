package com.moose.operator.web.service.impl.poetry;

import com.moose.operator.common.PageInfo;
import com.moose.operator.common.QueryWrapper;
import com.moose.operator.model.entity.PoetryTagInfo;
import com.moose.operator.model.param.SearchParam;
import com.moose.operator.web.service.PoetryTagInfoService;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author taohua
 */

@Slf4j
@Service
public class PoetryTagInfoServiceImpl implements PoetryTagInfoService {

  @Resource
  private MongoTemplate mongoTemplate;

  @Override public PageInfo<PoetryTagInfo> listPoetryTag(SearchParam searchParam) {
    Integer pageNum = searchParam.getPageNum();
    Integer pageSize = searchParam.getPageSize();
    Query query = QueryWrapper.buildPageQuery(pageNum, pageSize);
    query.with(Sort.by(Sort.Direction.DESC, "count"));

    PageInfo<PoetryTagInfo> pageInfo = new PageInfo<>();
    List<PoetryTagInfo> list = mongoTemplate.find(query, PoetryTagInfo.class);

    long count = mongoTemplate.count(query, PoetryTagInfo.class);
    pageInfo.setList(list);
    pageInfo.setTotalSize(count);
    return pageInfo;
  }

  @Override public PageInfo<PoetryTagInfo> tagListTop10() {
    SearchParam searchParam = new SearchParam();
    searchParam.setPageNum(1);
    searchParam.setPageSize(10);
    return this.listPoetryTag(searchParam);
  }
}
