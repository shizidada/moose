package com.moose.operator.web.service;

import com.moose.operator.model.domain.message.MessageRecordDO;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-02-05 17:16:17:16
 * @see com.moose.operator.web.service
 */
public interface MessageRecordService {

  /**
   * 获取聊天消息列表
   *
   * @return List<ChatMessageRecord>
   */
  Map<String, Object> listMessageRecord();

  /**
   * 查询聊天列表
   *
   * @param userId  对方 Id
   * @param pageNum 页码
   * @return 聊天列表详细
   */
  Map<String, Object> listMessageChat(Long userId, Integer pageNum);
}
