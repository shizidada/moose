package com.moose.operator.web.service;

import java.util.Map;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-20 20:09:20:09
 * @see com.moose.operator.web.service
 */
public interface BannerService {

  /**
   * 获取 banner 图
   *
   * @return List<BannerInfoDTO>
   */
  Map<String, Object> getMainBannerInfoList();
}
