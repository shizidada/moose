package com.moose.operator.web.service;

import com.moose.operator.model.domain.UserInfoDO;
import com.moose.operator.model.dto.UserInfoDTO;
import com.moose.operator.model.param.UserInfoParam;

/**
 * @author taohua
 */
public interface UserInfoService {

  /**
   * save User
   *
   * @param userInfoDO user info
   * @return 是否保存成功
   */
  boolean saveUserInfo(UserInfoDO userInfoDO);

  /**
   * 获取 User
   *
   * @param userId user id
   * @return User
   */
  UserInfoDTO getUserInfoByUserId(Long userId);

  /**
   * 获取用户信息
   *
   * @param accountId 账号 Id
   * @return 用户信息
   */
  UserInfoDTO getUserInfoByAccountId(Long accountId);

  /**
   * 更新用户信息
   *
   * @param userInfoParam 用户信息
   * @return update success
   */
  boolean updateUserInfo(UserInfoParam userInfoParam);

  /**
   * 获取用户信息
   *
   * @return 获取当前已经登录用户信息
   */
  UserInfoDTO getCurrentUserInfo();

  /**
   * 获取当前用户 Id
   *
   * @return 用户 Id
   */
  Long getCurrentUserId();

  /**
   * 重置手机号码
   *
   * @param phone   用户信息
   * @param smsCode 用户信息
   * @return 是否成功
   */
  boolean resetPhone(String phone, String smsCode);
}
