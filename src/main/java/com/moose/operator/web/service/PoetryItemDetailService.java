package com.moose.operator.web.service;

import com.moose.operator.model.entity.PoetryItemDetail;

/**
 * @author taohua
 */
public interface PoetryItemDetailService {

  /**
   * 诗词详细信息
   *
   * @param poetryId 诗词Id
   * @return 诗词详细信息
   */
  PoetryItemDetail getDetailByPoetryId(Integer poetryId);
}
