package com.moose.operator.web.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-03-27 22:40:22:40
 * @see com.moose.operator.web.service
 */
public interface QRCodeService {

  /**
   * 生成二维码，携带一个票据返回给客户端
   *
   * @param response HttpServletResponse
   * @param request  HttpServletRequest
   * @return 返回二维码
   */
  String getQRCode(HttpServletResponse response, HttpServletRequest request);

  /**
   * 更新扫描二维码状态
   *
   * @param request HttpServletRequest
   * @return 是否扫描成功
   */
  Integer askQRCodeStatus(HttpServletRequest request);

  /**
   * 扫描登录
   *
   * @param request
   */
  void scanLogin(HttpServletRequest request);
}
