package com.moose.operator.web.service;

import com.moose.operator.common.PageInfo;
import com.moose.operator.model.entity.HistoryBookChapter;
import com.moose.operator.model.entity.HistoryBookChapterDetail;
import com.moose.operator.model.param.book.HistoryBookChapterParam;

/**
 * @author taohua
 */
public interface HistoryBookChapterService {

  /**
   * 获取对应书籍章节
   *
   * @param searchParam 查询条件
   * @return PageInfo<HistoryBookChapter>
   */
  PageInfo<HistoryBookChapter> getChapterList(HistoryBookChapterParam searchParam);

  /**
   * 获取对应书籍章节
   *
   * @param chapterId 章节 Id
   * @return HistoryBookChapterDetail
   */
  HistoryBookChapterDetail getChapterDetail(Long chapterId);
}
