package com.moose.operator.web.service;

import com.moose.operator.common.PageInfo;
import com.moose.operator.model.entity.PoetryAuthorInfo;
import com.moose.operator.model.param.SearchParam;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-13 22:07:22:07
 * @see com.moose.operator.web.service
 */
public interface PoetryAuthorInfoService {
  /**
   * 诗词作者 列表
   *
   * @param searchParam searchVO
   *
   * @return List<PoetryAuthorInfo>
   */
  PageInfo<PoetryAuthorInfo> listPoetryAuthorInfo(SearchParam searchParam);
}
