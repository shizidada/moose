package com.moose.operator.web.service.impl.im;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.moose.operator.mapper.MessageRecordMapper;
import com.moose.operator.model.domain.message.MessageRecordDO;
import com.moose.operator.model.dto.message.MessageRecordDTO;
import com.moose.operator.socket.io.MessageUtils;
import com.moose.operator.util.PageInfoUtils;
import com.moose.operator.web.service.MessageRecordService;
import com.moose.operator.web.service.UserInfoService;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-02-05 17:16:17:16
 * @see com.moose.operator.web.service.impl.im
 */
@Slf4j
@Service
public class MessageRecordServiceImpl implements MessageRecordService {

  @Resource
  private UserInfoService userInfoService;

  @Resource
  private MessageRecordMapper messageRecordMapper;

  @Override public Map<String, Object> listMessageRecord() {
    // TODO: Mock 20 条
    PageHelper.startPage(1, 20, "send_time DESC");
    Long userId = userInfoService.getCurrentUserId();
    List<MessageRecordDO> messageRecordList = messageRecordMapper.selectMessageRecordList(userId);

    return buildMessageRecordResult(messageRecordList);
  }

  @Override public Map<String, Object> listMessageChat(Long userId, Integer pageNum) {
    Long currentUserId = userInfoService.getCurrentUserId();

    PageHelper.startPage(pageNum, 10, "send_time DESC");

    List<MessageRecordDO> messageRecordList =
        messageRecordMapper.selectChatMessageList(currentUserId, userId);

    return buildMessageRecordResult(messageRecordList);
  }

  private Map<String, Object> buildMessageRecordResult(List<MessageRecordDO> messageRecordList) {
    PageInfo<MessageRecordDO> pageInfo = new PageInfo<>(messageRecordList);
    List<MessageRecordDTO> messageRecordDTOList =
        messageRecordList.stream().map(this::convertDTOFromDO).collect(Collectors.toList());

    Map<String, Object> result = PageInfoUtils.getPageInfo(pageInfo);
    result.put("lists", messageRecordDTOList);
    return result;
  }

  /**
   * 转换 MessageRecordDTO
   *
   * @param messageRecordDO MessageRecordDO
   * @return MessageRecordDTO
   */
  private MessageRecordDTO convertDTOFromDO(MessageRecordDO messageRecordDO) {
    MessageRecordDTO messageRecordDTO = new MessageRecordDTO();
    messageRecordDTO.setMsgId(messageRecordDO.getMsgId());
    messageRecordDTO.setSendId(messageRecordDO.getSendId());
    messageRecordDTO.setReceiveId(messageRecordDO.getReceiveId());
    messageRecordDTO.setContent(messageRecordDO.getContent());
    messageRecordDTO.setType(MessageUtils.convertType(messageRecordDO.getType()));
    messageRecordDTO.setChatType(MessageUtils.convertChatType(messageRecordDO.getChatType()));
    messageRecordDTO.setSendTime(messageRecordDO.getSendTime());
    return messageRecordDTO;
  }
}
