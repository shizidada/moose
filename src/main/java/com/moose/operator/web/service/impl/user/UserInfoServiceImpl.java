package com.moose.operator.web.service.impl.user;

import com.moose.operator.exception.BusinessException;
import com.moose.operator.mapper.AccountMapper;
import com.moose.operator.mapper.UserInfoMapper;
import com.moose.operator.model.api.ResultCode;
import com.moose.operator.model.domain.UserInfoDO;
import com.moose.operator.model.dto.AccountDTO;
import com.moose.operator.model.dto.SmsCodeDTO;
import com.moose.operator.model.dto.UserInfoDTO;
import com.moose.operator.model.param.UserInfoParam;
import com.moose.operator.web.security.component.MooseUserDetails;
import com.moose.operator.web.service.AccountService;
import com.moose.operator.web.service.SmsCodeSenderService;
import com.moose.operator.web.service.UserInfoCacheService;
import com.moose.operator.web.service.UserInfoService;
import javax.annotation.Resource;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * mongodb operator Query query = Query.query(Criteria.where("account_id").is(accountId).and("account_name").is(accountName));
 * UserInfoDO userInfoDO = this.mongoTemplate.findOne(query, UserInfoDO.class);
 */

/**
 * @author taohua
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {

  @Resource
  private AccountService accountService;

  @Resource
  private SmsCodeSenderService smsCodeSenderService;

  @Resource
  private UserInfoCacheService userInfoCacheService;

  @Resource
  private UserInfoMapper userInfoMapper;

  @Resource
  private AccountMapper accountMapper;

  @Transactional(rollbackFor = Exception.class)
  @Override
  public boolean saveUserInfo(UserInfoDO userInfoDO) {
    if (!userInfoMapper.insertUserInfo(userInfoDO)) {
      throw new BusinessException(ResultCode.USER_INFO_SAVE_FAIL);
    }
    return Boolean.TRUE;
  }

  @Override public UserInfoDTO getUserInfoByAccountId(Long accountId) {
    UserInfoDO userInfoDO = userInfoMapper.findByAccountId(accountId);
    if (ObjectUtils.isEmpty(userInfoDO)) {
      return null;
    }

    UserInfoDTO userInfoDTO = new UserInfoDTO();
    BeanUtils.copyProperties(userInfoDO, userInfoDTO);
    return userInfoDTO;
  }

  @Override
  public UserInfoDTO getUserInfoByUserId(Long userId) {
    UserInfoDO userInfoDO = userInfoMapper.findByUserId(userId);
    if (ObjectUtils.isEmpty(userInfoDO)) {
      return null;
    }
    UserInfoDTO userInfoDTO = new UserInfoDTO();
    BeanUtils.copyProperties(userInfoDO, userInfoDTO);
    return userInfoDTO;
  }

  @Transactional(rollbackFor = Exception.class)
  @Override
  public boolean updateUserInfo(UserInfoParam userInfoParam) {
    if (ObjectUtils.isEmpty(userInfoParam)) {
      throw new BusinessException(ResultCode.USER_INFO_NOT_EXIST);
    }

    UserInfoDTO userInfoDTO = new UserInfoDTO();
    BeanUtils.copyProperties(userInfoParam, userInfoDTO);

    Object principal = accountService.getPrincipal();
    MooseUserDetails userDetails = (MooseUserDetails) principal;
    AccountDTO accountDTO = userDetails.getAccountDTO();
    Long accountId = accountDTO.getAccountId();

    if (!accountMapper.updateAccountNameByAccountId(userInfoDTO.getUserName(), accountId)) {
      throw new BusinessException(ResultCode.USER_INFO_UPDATE_FAIL);
    }

    UserInfoDO userInfoDO = new UserInfoDO();
    BeanUtils.copyProperties(userInfoDTO, userInfoDO);

    if (!userInfoMapper.updateUserInfoByAccountId(accountId, userInfoDO)) {
      throw new BusinessException(ResultCode.USER_INFO_UPDATE_FAIL);
    }
    return Boolean.TRUE;
  }

  @Override public UserInfoDTO getCurrentUserInfo() {
    Long accountId = accountService.getCurrentUserAccountId();
    UserInfoDTO userInfoDTO = userInfoCacheService.getUserInfoFromCacheByAccountId();
    if (!ObjectUtils.isEmpty(userInfoDTO)) {
      return userInfoDTO;
    }
    userInfoDTO = this.getUserInfoByAccountId(accountId);
    if (ObjectUtils.isEmpty(userInfoDTO)) {
      throw new BusinessException(ResultCode.USER_INFO_NOT_EXIST);
    }
    userInfoCacheService.saveUserInfoToCacheByAccountId(userInfoDTO);
    return userInfoDTO;
  }

  @Override public Long getCurrentUserId() {
    return getCurrentUserInfo().getUserId();
  }

  // TODO: 变更手机号码
  @Transactional(rollbackFor = Exception.class)
  @Override public boolean resetPhone(String phone, String smsCode) {
    if (StringUtils.isEmpty(phone)) {
      throw new BusinessException(ResultCode.PHONE_NUMBER_IS_EMPTY);
    }

    if (StringUtils.isEmpty(smsCode)) {
      throw new BusinessException(ResultCode.SMS_CODE_IS_EMPTY);
    }

    Long accountId = accountService.getCurrentUserAccountId();

    UserInfoDTO userInfoDTO = this.getUserInfoByAccountId(accountId);
    if (StringUtils.equals(phone, userInfoDTO.getPhone())) {
      smsCodeSenderService.setSmsCodeCacheExpire(phone);
      throw new BusinessException(ResultCode.PHONE_EXITS_WITH_CURRENT);
    }

    SmsCodeDTO smsCodeDTO = smsCodeSenderService.getSmsCodeFromCacheByPhone(phone);
    if (ObjectUtils.isEmpty(smsCodeDTO) || smsCodeDTO.getExpired()) {
      throw new BusinessException(ResultCode.SMS_CODE_ERROR);
    }

    if (accountService.updateAccountPhone(accountId, phone)) {
      if (!userInfoMapper.updatePhoneByAccountId(accountId, phone)) {
        throw new BusinessException(ResultCode.PHONE_RESET_FAIL);
      }
    }

    // update cache user info
    userInfoDTO.setPhone(phone);
    return Boolean.TRUE;
  }
}
