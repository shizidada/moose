package com.moose.operator.web.service;

import com.moose.operator.common.PageInfo;
import com.moose.operator.model.entity.PoetryItem;
import com.moose.operator.model.param.SearchParam;
import com.moose.operator.model.param.poetry.PoetrySearchParam;
import java.util.List;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-13 21:46:21:46
 * @see com.moose.operator.web.service
 */
public interface PoetryItemService {

  /**
   * 诗词
   *
   * @param searchParam 查询条件
   * @return List<PoetryItem>
   */
  PageInfo<PoetryItem> listPoetry(PoetrySearchParam searchParam);

  /**
   * 诗词排名
   *
   * @param searchParam 查询参数
   *
   * @return List<PoetryItem>
   */
  PageInfo<PoetryItem> listPoetryRanking(SearchParam searchParam);
}
