package com.moose.operator.web.service.impl.user;

import com.moose.operator.constant.CacheNameConstant;
import com.moose.operator.model.dto.UserInfoDTO;
import com.moose.operator.util.RedisHelper;
import com.moose.operator.web.service.AccountService;
import com.moose.operator.web.service.UserInfoCacheService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-10-17 20:32:20:32
 * @see com.moose.operator.web.service.impl
 */
@Service
public class UserInfoCacheServiceImpl implements UserInfoCacheService {

  @Resource
  private RedisHelper redisHelper;

  @Resource
  private AccountService accountService;

  @Override
  public void saveUserInfoToCacheByAccountId(UserInfoDTO userInfoDTO) {
    Long accountId = accountService.getCurrentUserAccountId();
    String userInfoKey = String.format(CacheNameConstant.USER_INFO_KEY, accountId);
    redisHelper.cacheValue(userInfoKey, userInfoDTO);
  }

  @Override public UserInfoDTO getUserInfoFromCacheByAccountId() {
    Long accountId = accountService.getCurrentUserAccountId();
    String userInfoKey = String.format(CacheNameConstant.USER_INFO_KEY, accountId);
    return (UserInfoDTO) redisHelper.getValue(userInfoKey);
  }

  @Override public void removeCacheUserInfoByAccountId() {
    Long accountId = accountService.getCurrentUserAccountId();
    String userInfoKey = String.format(CacheNameConstant.USER_INFO_KEY, accountId);
    redisHelper.removeValue(userInfoKey);
  }
}
