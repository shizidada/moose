//package com.moose.operator.web.websocket;
//
//import com.moose.operator.common.SnowflakeIdWorker;
//import com.moose.operator.constant.ChatMessageConstant;
//import com.moose.operator.model.dto.ChatMessageDTO;
//import com.moose.operator.model.dto.MessageInfoDTO;
//import com.moose.operator.model.dto.UserInfoDTO;
//import com.moose.operator.util.MapperUtils;
//import com.moose.operator.web.service.ChatMessageService;
//import com.moose.operator.web.service.UserInfoService;
//import java.time.LocalDateTime;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//import javax.annotation.Resource;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.ObjectUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.socket.CloseStatus;
//import org.springframework.web.socket.TextMessage;
//import org.springframework.web.socket.WebSocketMessage;
//import org.springframework.web.socket.WebSocketSession;
//import org.springframework.web.socket.handler.TextWebSocketHandler;
//
///**
// * @author taohua
// */
//@Slf4j
//@Component
//public class MooseMessageWebSocketHandler extends TextWebSocketHandler {
//
//  /**
//   * 用户存放用户的在线 session
//   */
//  private static final Map<String, WebSocketSession> SESSION_MAP = new ConcurrentHashMap<>(16);
//
//  @Autowired
//  private UserInfoService userService;
//
//  @Autowired
//  private ChatMessageService chatMessageService;
//
//  @Resource
//  private SnowflakeIdWorker snowflakeIdWorker;
//
//  /**
//   * 开启连接
//   *
//   * @throws Exception
//   */
//  @Override
//  public void afterConnectionEstablished(WebSocketSession session) throws Exception {
//    // 将当前用户的 session 放置到 map 中，后面会使用相应的 session 通信
//    String userId = (String) session.getAttributes().get("userId");
//    SESSION_MAP.put(userId, session);
//  }
//
//  @Override
//  protected void handleTextMessage(WebSocketSession session, TextMessage textMessage)
//      throws Exception {
//    try {
//      // 将消息序列化成JSON串
//      String messagePayloadJson = textMessage.getPayload();
//      if (StringUtils.isEmpty(messagePayloadJson)) {
//        log.info("Parse Message payload json is Empty :: [{}]", messagePayloadJson);
//        return;
//      }
//
//      MessageInfoDTO messageInfo = MapperUtils.json2pojo(messagePayloadJson, MessageInfoDTO.class);
//      if (ObjectUtils.isEmpty(messageInfo)) {
//        log.info("Parse Message info is Empty :: [{}]", messageInfo);
//        return;
//      }
//
//      // TODO: check toId, the user is exist
//      Long toUserId = messageInfo.getToId();
//      String message = messageInfo.getMessage();
//      if (ObjectUtils.isEmpty(toUserId) || StringUtils.isEmpty(message)) {
//        log.info("Parse toId is Empty or Message is Empty :: [{}: {}]", toUserId, message);
//        return;
//      }
//
//      UserInfoDTO toUser = userService.getUserInfoByUserId(toUserId);
//      if (ObjectUtils.isEmpty(toUser)) {
//        log.info("Parse ToUser is Empty :: [{}]", messageInfo);
//        return;
//      }
//
//      Long fromUserId = Long.valueOf((String) session.getAttributes().get("userId"));
//      UserInfoDTO fromUser = userService.getUserInfoByUserId(fromUserId);
//      if (ObjectUtils.isEmpty(fromUser)) {
//        log.info("Parse FromUser is Empty :: [{}]", messageInfo);
//        return;
//      }
//
//      // 构造消息对象
//      ChatMessageDTO chatMessageDTO = new ChatMessageDTO();
//      chatMessageDTO.setMessageId(snowflakeIdWorker.nextId());
//      chatMessageDTO.setFrom(fromUser);
//      chatMessageDTO.setTo(toUser);
//      chatMessageDTO.setMessage(message);
//      chatMessageDTO.setIsRead(ChatMessageConstant.MESSAGE_UN_READ);
//      chatMessageDTO.setSendTime(LocalDateTime.now());
//
//      // 将消息保存到 mongodb 数据库中
//      log.info("The chat message :: [{}]", chatMessageDTO);
//
//      //chatMessageDTO = this.chatMessageService.saveMessage(chatMessageDTO);
//      //if (ObjectUtils.isEmpty(chatMessageDTO)) {
//      //  log.info("save chat message fail :: [{}]", chatMessageDTO);
//      //  return;
//      //}
//      //// 判断 to 用户是否在线
//      //WebSocketSession socketSession = SESSION_MAP.get(toUserId);
//      //if (ObjectUtils.isNotEmpty(socketSession) && socketSession.isOpen()) {
//      //  // TODO: 根据前端的格式对接
//      //  // 响应消息
//      //  socketSession.sendMessage(new TextMessage(MapperUtils.obj2json(chatMessageDTO)));
//      //
//      //  // TODO: 更改消息状态
//      //}
//    } catch (Exception e) {
//      log.error("send message error: ", e);
//    }
//  }
//
//  /**
//   * 断开连接
//   *
//   * @throws Exception
//   */
//  @Override
//  public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
//    // 用户断开移除用户 Session
//    String userId = (String) session.getAttributes().get("userId");
//    SESSION_MAP.remove(userId);
//  }
//
//  /**
//   * @param targetId 发送给那个
//   * @param message  消息体
//   * @throws Exception 异常
//   */
//  public void sendMessage(String targetId, WebSocketMessage<?> message)
//      throws Exception {
//    if (message instanceof TextMessage) {
//      WebSocketSession socketSession = SESSION_MAP.get(targetId);
//      if (ObjectUtils.isNotEmpty(socketSession) && socketSession.isOpen()) {
//        socketSession.sendMessage(message);
//      }
//    }
//  }
//}
