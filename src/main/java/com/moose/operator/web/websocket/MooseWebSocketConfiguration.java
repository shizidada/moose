//package com.moose.operator.web.websocket;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.socket.config.annotation.EnableWebSocket;
//import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
//import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
//
///**
// * @author taohua
// */
//@Configuration
//@EnableWebSocket
//public class MooseWebSocketConfiguration implements WebSocketConfigurer {
//
//  @Autowired
//  private MooseMessageWebSocketHandler mooseMessageWebSocketHandler;
//
//  @Autowired
//  private MooseMessageHandshakeInterceptor mooseMessageHandshakeInterceptor;
//
//  @Override
//  public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
//    registry.addHandler(this.mooseMessageWebSocketHandler, "/socket.io/{userId}")
//        .setAllowedOrigins("*")
//        .addInterceptors(this.mooseMessageHandshakeInterceptor);
//  }
//}
