package com.moose.operator.api;

import com.moose.operator.feign.DefaultErrorDecoder;
import feign.Feign;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author taohua
 */
@Configuration
public class ApiConfiguration {

  @Bean
  public OAuth2Api oAuth2Api() {
    return Feign.builder()
        .errorDecoder(new DefaultErrorDecoder())
        .target(OAuth2Api.class, "http://127.0.0.1:7000");
  }
}
