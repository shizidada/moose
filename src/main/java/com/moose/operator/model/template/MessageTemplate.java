package com.moose.operator.model.template;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.moose.operator.annotation.ValueIn;
import com.moose.operator.model.emun.ChatTypeEnum;
import com.moose.operator.model.emun.MessageTypeEnum;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-01-24 16:36:16:36
 * @see com.moose.operator.model.template
 */
@Setter
@Getter
@ToString
public class MessageTemplate implements Serializable {

  @NotBlank(message = "消息类型不能为空")
  @ValueIn(value = MessageTypeEnum.class, message = "消息类型不正确")
  private String type;

  @NotBlank(message = "聊天类型不能为空")
  @ValueIn(value = ChatTypeEnum.class, message = "聊天类型不正确")
  private String chatType;

  @NotBlank(message = "消息类型不能为空")
  private String content;

  @NotBlank(message = "发送 ID 不能为空")
  @Pattern(regexp = "^[0-9]*$")
  @JsonSerialize(using = ToStringSerializer.class)
  private Long sendId;

  @NotBlank(message = "目标 ID 不能为空")
  @Pattern(regexp = "^[0-9]*$")
  @JsonSerialize(using = ToStringSerializer.class)
  private Long receiveId;
}
