package com.moose.operator.model.domain;

import lombok.Data;

/**
 * @author taohua
 */
@Data
public class DynamicRecordDO extends BaseDO {

  private Long drId;

  private Long userId;

  private String title;

  private String content;

  private UserInfoDO author;
}
