package com.moose.operator.model.domain;

import lombok.Data;

/**
 * @author taohua
 */
@Data
public class AttachmentRecordDO extends BaseDO {

  private Long attachId;

  private Long userId;

  private String eTag;

  private String fileUrl;
}
