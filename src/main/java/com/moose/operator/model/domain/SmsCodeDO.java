package com.moose.operator.model.domain;

import lombok.Data;

/**
 * @author taohua
 */
@Data
public class SmsCodeDO extends BaseDO {

  private Long smsId;

  private String phone;

  private String type;

  private String code;
}
