package com.moose.operator.model.domain;

import lombok.Data;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-10-21 21:57:21:57
 * @see com.moose.operator.model.domain
 */
@Data
public class DynamicRecordAttachmentRelationDO extends BaseDO {

  private Long draId;

  private Long attachId;

  private Long drId;
}
