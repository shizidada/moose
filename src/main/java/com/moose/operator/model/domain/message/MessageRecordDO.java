package com.moose.operator.model.domain.message;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.moose.operator.model.domain.BaseDO;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * @author taohua
 */

@Data
public class MessageRecordDO extends BaseDO {
  @JsonSerialize(using = ToStringSerializer.class)
  private Long msgId;

  /**
   * 发送者 id
   */
  @JsonSerialize(using = ToStringSerializer.class)
  private Long sendId;

  /**
   * 接受者 id
   */
  @JsonSerialize(using = ToStringSerializer.class)
  private Long receiveId;

  /**
   * 消息类型
   */
  private Integer type;

  /**
   * 列表类型
   */
  private Integer chatType;

  /**
   * 聊天 内容
   */
  private String content;

  private LocalDateTime sendTime;
}
