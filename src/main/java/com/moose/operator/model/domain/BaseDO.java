package com.moose.operator.model.domain;

import java.time.LocalDateTime;
import lombok.Data;

/**
 * <p>
 * Description
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2019 2019/11/17 20:43
 * @see com.moose.operator.model.domain
 */
@Data
public class BaseDO {

  /**
   * 创建时间
   */
  private LocalDateTime createTime;

  /**
   * 修改时间
   */
  private LocalDateTime updateTime;
}
