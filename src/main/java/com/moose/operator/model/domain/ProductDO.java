package com.moose.operator.model.domain;

import java.math.BigDecimal;
import lombok.Data;

/**
 * @author taohua
 */
@Data
public class ProductDO {

  private Long userId;

  private Long productId;

  private String productName;

  private BigDecimal productPrice;

  private String description;
}
