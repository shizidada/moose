package com.moose.operator.model.domain;

import java.time.LocalDateTime;
import lombok.Data;

/**
 * @author taohua
 */
@Data
public class ChatMessageDO extends BaseDO {

  private Long messageId;

  private String message;

  private Integer isRead;

  private LocalDateTime sendTime;

  private LocalDateTime readTime;

  private UserInfoDO to;

  private UserInfoDO from;
}
