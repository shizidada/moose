package com.moose.operator.model.domain;

import lombok.Data;

/**
 * @author taohua
 */
@Data
public class UserInfoDO extends BaseDO {

  private Long userId;

  private String userName;

  private Long accountId;

  private String accountName;

  private String phone;

  private String avatar;

  private String email;

  private String job;

  private String address;

  private String description;

  private String gender;
}