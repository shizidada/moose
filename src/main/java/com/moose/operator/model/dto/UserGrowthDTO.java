package com.moose.operator.model.dto;

import lombok.Data;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-10-28 22:34:22:34
 * @see com.moose.operator.model.dto
 */
@Data
public class UserGrowthDTO extends BaseDTO {

  private Long ugId;

  private Long userId;

  private Integer growth;
}
