package com.moose.operator.model.dto;

import lombok.Data;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-20 20:09:20:09
 * @see com.moose.operator.model.dto
 */

@Data
public class BannerInfoDTO {

  private String imagePath;
}
