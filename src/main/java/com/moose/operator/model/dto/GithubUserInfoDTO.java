package com.moose.operator.model.dto;

import lombok.Data;

/**
 * @author taohua
 */

@Data
public class GithubUserInfoDTO {
  private String id;

  private String avatarUrl;

  private String name;

  private String bio;

  private String email;
}
