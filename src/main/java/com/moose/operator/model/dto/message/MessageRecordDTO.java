package com.moose.operator.model.dto.message;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.moose.operator.model.dto.BaseDTO;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * @author taohua
 */
@Data
public class MessageRecordDTO extends BaseDTO implements Serializable {

  @JsonSerialize(using = ToStringSerializer.class)
  private Long msgId;

  @JsonSerialize(using = ToStringSerializer.class)
  private Long sendId;

  @JsonSerialize(using = ToStringSerializer.class)
  private Long receiveId;

  /**
   * 消息类型
   */
  private String type;

  /**
   * 列表类型
   */
  private String chatType;

  /**
   * 聊天 内容
   */
  private String content;

  private LocalDateTime sendTime;
}
