package com.moose.operator.model.dto;

import lombok.Data;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-03-17 21:43:21:43
 * @see com.moose.operator.model.dto
 */

@Data
public class AttachmentRecordDTO {

  private Long attachId;

  private Long userId;

  private String eTag;

  private String fileUrl;
}
