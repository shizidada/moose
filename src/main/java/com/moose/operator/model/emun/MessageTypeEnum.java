package com.moose.operator.model.emun;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-01-24 16:48:16:48
 * @see com.moose.operator.model.emun
 */
public enum MessageTypeEnum {

  /**
   * 文本消息
   */
  TEXT("MS:TEXT"),

  /**
   * 图片消息
   */
  IMAGE("MS:IMAGE"),

  /**
   * 文件消息
   */
  FILE("MS:FILE"),

  /**
   * 位置消息
   */
  LOCATION("MS:LOCATION"),

  /**
   * 视频消息
   */
  VIDEO("MS:VIDEO"),

  /**
   * 语音消息
   */
  VOICE("MS:VOICE"),

  UN_KNOW("MS:UN_KNOW");

  private final String value;

  MessageTypeEnum(String value) {
    this.value = value;
  }

  public static boolean isExist(String value) {
    if (StringUtils.isEmpty(value)) {
      return Boolean.FALSE;
    }
    for (MessageTypeEnum typeEnum : MessageTypeEnum.values()) {
      if (StringUtils.equals(typeEnum.value, value)) {
        return Boolean.TRUE;
      }
    }
    return Boolean.FALSE;
  }

  public String getValue() {
    return value;
  }
}
