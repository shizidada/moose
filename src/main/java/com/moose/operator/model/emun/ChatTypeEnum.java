package com.moose.operator.model.emun;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-01-24 16:48:16:48
 * @see com.moose.operator.model.emun
 */
public enum ChatTypeEnum {

  /**
   * 单聊
   */
  SINGLE("CT:SINGLE"),

  /**
   * 群聊
   */
  GROUP("CT:GROUP"),

  /**
   * 群聊
   */
  UN_KNOW("CT:UN_KNOW");

  private final String value;

  ChatTypeEnum(String value) {
    this.value = value;
  }

  public static boolean isExist(String value) {
    if (StringUtils.isEmpty(value)) {
      return Boolean.FALSE;
    }
    for (ChatTypeEnum typeEnum : ChatTypeEnum.values()) {
      if (StringUtils.equals(typeEnum.value, value)) {
        return Boolean.TRUE;
      }
    }
    return Boolean.FALSE;
  }

  public String getValue() {
    return value;
  }
}
