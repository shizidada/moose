package com.moose.operator.model.emun;

import org.apache.commons.lang3.StringUtils;

/**
 * @author taohua
 */

public enum AuthTypeEnum {

  /**
   * github
   */
  OAUTH_GITHUB("github");

  private final String value;

  AuthTypeEnum(String value) {
    this.value = value;
  }

  public static boolean isExist(String value) {
    if (StringUtils.isEmpty(value)) {
      return Boolean.FALSE;
    }
    for (AuthTypeEnum authTypeEnum : AuthTypeEnum.values()) {
      if (StringUtils.equals(authTypeEnum.value, value)) {
        return Boolean.TRUE;
      }
    }
    return Boolean.FALSE;
  }

  public String getValue() {
    return value;
  }
}
