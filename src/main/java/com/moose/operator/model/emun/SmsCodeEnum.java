package com.moose.operator.model.emun;

import com.moose.operator.constant.SmsTypeConstant;
import org.apache.commons.lang3.StringUtils;

/**
 * @author taohua
 */

public enum SmsCodeEnum {

  /**
   * 注册
   */
  REGISTER(SmsTypeConstant.REGISTER),

  /**
   * sms login
   */
  SMS_LOGIN(SmsTypeConstant.SMS_LOGIN),

  /**
   * reset phone number
   */
  RESET_PHONE(SmsTypeConstant.RESET_PHONE),

  /**
   * 重置密码
   */
  RESET_PASSWORD(SmsTypeConstant.RESET_PASSWORD);

  private final String value;

  SmsCodeEnum(String value) {
    this.value = value;
  }

  public static boolean isExist(String value) {
    if (StringUtils.isEmpty(value)) {
      return Boolean.FALSE;
    }
    for (SmsCodeEnum smsCodeEnum : SmsCodeEnum.values()) {
      if (StringUtils.equals(smsCodeEnum.value, value)) {
        return Boolean.TRUE;
      }
    }
    return Boolean.FALSE;
  }

  public String getValue() {
    return value;
  }
}
