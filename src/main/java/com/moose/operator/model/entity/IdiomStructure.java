package com.moose.operator.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author taohua
 */
@Data
@Document(collection = "idiom_structure")
public class IdiomStructure extends BaseEntity {

  /**
   * 词语结构 中文
   */
  @Field(value = "cn_name")
  private String cnName;

  /**
   * 词语结构 拼音
   */
  @Field(value = "py_name")
  private String pyName;
}
