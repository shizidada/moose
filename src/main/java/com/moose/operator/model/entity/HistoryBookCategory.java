package com.moose.operator.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author taohua
 */
@Data
@Document(collection = "history_book_category")
public class HistoryBookCategory extends BaseEntity {

  /**
   * 历史书籍分类 id
   */
  @Field(value = "category_id")
  private Long categoryId;
  /**
   * 历史书籍 cn 名称
   */
  @Field(value = "category_name")
  private String categoryName;
  /**
   * 历史书籍 py 名称
   */
  @Field(value = "category_py")
  private String categoryPy;
  /**
   * 历史书籍 描述
   */
  @Field(value = "description")
  private String description;
}
