package com.moose.operator.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-31 20:44:20:44
 * @see com.moose.operator.model.entity
 */
@Data
@Document(collection = "poetry_author_detail_info")
public class PoetryAuthorDetailInfo extends BaseEntity {

  /**
   * 诗人 id
   */
  @Field("author_id")
  private Integer authorId;

  /**
   * 轶事典故
   */
  @Field("allusion")
  private String allusion;

  /**
   * 人物生平
   */
  @Field("life")
  private String life;

  /**
   * 成就
   */
  @Field("achievement")
  private String achievement;

  /**
   * 评价
   */
  @Field("evaluation")
  private String evaluation;
}
