package com.moose.operator.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-13 21:38:21:38
 * @see com.moose.operator.model.entity
 */
@Data
@Document(collection = "poetry_author_info")
public class PoetryAuthorInfo extends BaseEntity {

  /**
   * 作者 id
   */
  @Field("id")
  private Integer id;

  /**
   * 简介
   */
  @Field("intro")
  private String intro;

  /**
   * 朝代
   */
  @Field("dynasty")
  private String dynasty;

  /**
   * 朝代 id
   */
  @Field("dynasty_id")
  private Integer dynastyId;

  /**
   * 收录诗词数量
   */
  @Field("num")
  private Integer collected;

  /**
   * 作者姓名
   */
  @Field("author")
  private String authorName;
}
