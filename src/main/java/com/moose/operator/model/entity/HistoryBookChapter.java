package com.moose.operator.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author taohua
 */
@Data
@Document(collection = "history_book_chapter")
public class HistoryBookChapter extends BaseEntity {

  /**
   * 是否是分类 标题 is_category_title" : 0,
   */
  @Field(value = "is_category_title")
  private Integer isCategoryTitle;

  /**
   * 章节名字 "name" : "第一回·宴桃园豪杰三结义  斩黄巾英雄首立功",
   */
  @Field(value = "name")
  private String name;

  /**
   * 章节 排序 编号 sort "":1,
   */

  @Field(value = "sort")
  private Integer sort;

  /**
   * 章节对应 书籍 Id "book_id":793077399121301504,
   */
  @Field(value = "book_id")
  private Long bookId;

  /**
   * 章节 Id "chapter_id":793077408453627904,
   */
  @Field(value = "chapter_id")
  private Long chapterId;
}
