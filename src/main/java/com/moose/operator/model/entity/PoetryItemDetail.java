package com.moose.operator.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author taohua
 */

@Data
@Document(collection = "poetry_item_detail")
public class PoetryItemDetail extends BaseEntity {
  @Field("poetry_id")
  private Integer poetryId;

  /**
   * 鉴赏
   */
  @Field("appreciation")
  private String appreciation;

  /**
   * 说明
   */
  @Field("explanation")
  private String explanation;

  /**
   * 创作背景
   */
  @Field("historical")
  private String historical;

  /**
   * 诗词翻译
   */
  @Field("translation")
  private String translation;

  /**
   * 译文及注释
   */
  @Field("yi_zhu")
  private String yiZhu;

  private String body;
}
