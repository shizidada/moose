package com.moose.operator.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * <p>
 * Description
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2019 2019/11/17 20:43
 * @see com.moose.operator.model.domain
 */
@Data
public class BaseEntity {

  /**
   * 创建时间
   */
  @Field("created_time")
  private Long createTime;

  /**
   * 修改时间
   */
  @Field("updated_time")
  private Long updateTime;
}
