package com.moose.operator.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author taohua
 */
@Data
@Document(collection = "idiom_type")
public class IdiomType extends BaseEntity {

  /**
   * 词语类型 中文
   */
  @Field(value = "cn_name")
  private String cnName;

  /**
   * 词语类型 拼音
   */
  @Field(value = "py_name")
  private String pyName;
}
