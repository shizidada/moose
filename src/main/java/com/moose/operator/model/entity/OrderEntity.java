package com.moose.operator.model.entity;

import lombok.Data;

/**
 * @author taohua
 */
@Data
public class OrderEntity extends BaseEntity {
  /**
   * 主键 id
   */
  private Long id;

  /**
   * 收货人
   */
  private String receiver;

  /**
   * 收货手机号
   */
  private String phone;

  /**
   * 收货地址
   */
  private String address;
}
