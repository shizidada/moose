package com.moose.operator.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author taohua
 */

@Data
@Document(collection = "history_book")
public class HistoryBook extends BaseEntity {

  @Field(value = "book_id")
  private Long bookId;

  @Field(value = "book_name")
  private String bookName;

  @Field(value = "dynasty")
  private String dynasty;

  @Field(value = "author_name")
  private String authorName;

  @Field(value = "description")
  private String description;

  @Field(value = "book_category_id")
  private Long bookCategoryId;

  @Field(value = "has_chapter_category")
  private Integer hasChapterCategory;
}
