package com.moose.operator.model.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author taohua
 * <p>
 * 消息记录
 */
@Data
@Document(collection = "chat_message_record")
public class ChatMessageRecord {

  /**
   *  msg_id // 消息ID
   *  type // 消息类型 TEXT -> 0, IMAGE -> 1, FILE -> 2, LOCATION -> 3, VIDEO -> 4, VOICE -> 5
   *  chat_type // 聊天类型 SINGLE -> 0, GROUP -> 1
   *  last_msg_content // 最后接受 消息
   *  user_id // 用户 ID
   *  from_user_id // 发送者 ID
   *  to_user_id // 接受者 ID
   *  un_read_count // 未读消息数量
   *  send_time // 发送时间 => 服务器收到消息时间
   */

  @Id
  private String id;

  @Field(value = "msg_id")
  @JsonSerialize(using = ToStringSerializer.class)
  private Long msgId;

  @Field(value = "type")
  private Integer type;

  @Field(value = "chat_type")
  private Integer chatType;

  @Field(value = "last_msg_content")
  private String lastMsgContent;

  @Field(value = "user_id")
  @JsonSerialize(using = ToStringSerializer.class)
  private Long userId;

  @Field(value = "from_user_id")
  @JsonSerialize(using = ToStringSerializer.class)
  private Long fromUserId;

  @Field(value = "to_user_id")
  @JsonSerialize(using = ToStringSerializer.class)
  private Long toUserId;

  @Field(value = "un_read_count")
  private Integer unReadCount;

  @Field(value = "send_time")
  private Long sendTime;
}
