package com.moose.operator.model.entity;

import java.util.List;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author taohua
 * @Document 和 mongodb 集合名称对应
 */
@Data
@Document(collection = "poetry_item")
public class PoetryItem extends BaseEntity {
  /**
   * 别名
   */
  @Field("alias")
  private String alias;

  /**
   * 作者
   */
  @Field("author")
  private String author;

  /**
   * 作者 id
   */
  @Field("author_id")
  private Integer authorId;

  /**
   * 诗词主题内容
   */
  @Field("body")
  private String body;

  /**
   * 朝代 id
   */
  @Field("dynasty_id")
  private Integer dynastyId;

  /**
   * 朝代
   */
  @Field("dynasty")
  private String dynasty;

  /**
   * 体裁 五律 七律 五绝
   */
  @Field("genre")
  private String genre;

  /**
   * 诗词 id
   */
  @Field("id")
  private Integer id;

  /**
   * 标签对应标签 id
   */
  @Field("tag_arr")
  private List<Object> tags;

  /**
   * 诗词名称
   */
  @Field("title")
  private String title;

  /**
   * 类型 诗 词 曲 文言文
   */
  @Field("type")
  private String type;

  /**
   * 诗词排名
   */
  @Field("ranking_num")
  private Integer rankingNum;
}
