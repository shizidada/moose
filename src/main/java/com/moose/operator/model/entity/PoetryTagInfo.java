package com.moose.operator.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author taohua
 */

@Data
@Document(collection = "poetry_tag_info")
public class PoetryTagInfo {

  @Field("id")
  private Integer id;

  @Field("tag_name")
  private String tagName;

  @Field("count")
  private Integer total;
}
