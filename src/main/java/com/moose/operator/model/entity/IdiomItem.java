package com.moose.operator.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author taohua
 */
@NoArgsConstructor @Data
@Document(collection = "idiom_item")
public class IdiomItem extends BaseEntity {

  /**
   * 成语 id
   */
  @Field(value = "id")
  private Integer id;

  /**
   * 成语
   */
  @Field(value = "chengyu")
  private String chengyu;

  /**
   * 成语 拼音第一个字
   */
  @Field(value = "f_pinyin")
  private String fPinyin;

  /**
   * 形式 aabb
   */
  @Field(value = "xingshi")
  private String xingshi;

  /**
   * 读音
   */
  @Field(value = "duyin")
  private String duyin;

  /**
   * 解释
   */
  @Field(value = "jieshi")
  private String jieshi;

  /**
   * 成语出处
   */
  @Field(value = "chuchu")
  private String chuchu;

  /**
   * 成语造句
   */
  @Field(value = "zaoju")
  private String zaoju;

  /**
   * 成语用法
   */
  @Field(value = "yongfa")
  private String yongfa;

  /**
   * 感情色彩
   */
  @Field(value = "secai")
  private String secai;

  /**
   * 成语结构 binglieshi
   */
  @Field(value = "jiegou")
  private String jiegou;

  /**
   * 名著
   */
  @Field(value = "mingzhu")
  private String mingzhu;

  /**
   * 名人
   */
  @Field(value = "mingren")
  private String mingren;

  /**
   * 十二生肖
   */
  @Field(value = "shiershengxiao")
  private String shiershengxiao;

  /**
   * 人物
   */
  @Field(value = "renwu")
  private String renwu;

  /**
   * 心情
   */
  @Field(value = "xinqing")
  private String xinqing;

  /**
   * 实物
   */
  @Field(value = "shiwu")
  private String shiwu;

  /**
   * 常用程度
   */
  @Field(value = "chengdu")
  private String chengdu;

  /**
   * 繁体
   */
  @Field(value = "fanti")
  private String fanti;

  /**
   * 近义词
   */
  @Field(value = "jinyici")
  private String jinyici;

  /**
   * 反义词
   */
  @Field(value = "fanyici")
  private String fanyici;

  ///**
  // * 朝代
  // */
  //@Field(value = "chaodai")
  //private String chaodai;

  ///**
  // * 动物
  // */
  //@Field(value = "dongwu")
  //private String dongwu;

  //@Field(value = "jingshe")
  //private String jingshe;

  /// @Field(value = "zhengyin")
  /// private String zhengyin;

  ///@Field(value = "index")
  ///private String index;

  ///*
  // * 成语反义词 一三位反义词 yisanweifanyici
  // * @Field(value = "cy_fanyici")
  // * private String cyFanyici;
  // */
}
