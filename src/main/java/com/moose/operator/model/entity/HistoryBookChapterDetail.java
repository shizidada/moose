package com.moose.operator.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author taohua
 */

@Data
@Document(collection = "history_book_chapter_detail")
public class HistoryBookChapterDetail extends BaseEntity {

  /**
   * 章节名字 "title" : "第一回·宴桃园豪杰三结义  斩黄巾英雄首立功",
   */
  @Field(value = "title")
  private String title;

  /**
   * 章节内容 "content" : "xxx",
   */
  @Field(value = "content")
  private String content;

  /**
   * 章节对应 书籍 Id "book_id":793077399121301504,
   */
  @Field(value = "book_id")
  private Long bookId;

  /**
   * 章节 Id "chapter_id":793077408453627904,
   */
  @Field(value = "chapter_id")
  private Long chapterId;
}
