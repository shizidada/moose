package com.moose.operator.model.param.book;

import com.moose.operator.model.param.SearchParam;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * @author taohua
 */
@Data
public class HistoryBookChapterParam extends SearchParam {

  @NotNull(message = "书籍 Id 不能为空")
  private Long bookId;
}
