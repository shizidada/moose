package com.moose.operator.model.param;

import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @author taohua
 */
@Data
public class AttachmentParam {

  @NotBlank(message = "附件标识不能为空")
  private String tag;
}
