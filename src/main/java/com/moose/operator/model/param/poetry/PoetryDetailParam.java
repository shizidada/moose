package com.moose.operator.model.param.poetry;

import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * @author taohua
 */
@Data
public class PoetryDetailParam {

  @NotNull(message = "诗词 Id 不能为空")
  private Integer poetryId;
}
