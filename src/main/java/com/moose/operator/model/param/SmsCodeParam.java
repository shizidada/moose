package com.moose.operator.model.param;

import com.moose.operator.annotation.ValueIn;
import com.moose.operator.model.emun.SmsCodeEnum;
import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-04-01 17:15:17:15
 * @see com.moose.operator.model.vo
 */
@Data
public class SmsCodeParam {

  @NotBlank(message = "手机号码不能为空")
  private String phone;

  @NotBlank(message = "短信类型不能为空")
  @ValueIn(value = SmsCodeEnum.class, message = "短信类型不正确")
  private String type;
}
