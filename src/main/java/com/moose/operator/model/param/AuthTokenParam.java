package com.moose.operator.model.param;

import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @author taohua
 */
@Data
public class AuthTokenParam {
  @NotBlank(message = "access token 不能为空")
  private String accessToken;
}
