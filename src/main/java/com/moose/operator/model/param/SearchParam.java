package com.moose.operator.model.param;

import com.moose.operator.constant.PageInfoConstant;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import lombok.Data;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-10-19 22:56:22:56
 * @see com.moose.operator.model.vo
 */
@Data
public class SearchParam {

  /**
   * 页数
   */
  @Min(value = 1, message = "pageNum 最小为 1")
  private Integer pageNum = PageInfoConstant.DEFAULT_PAGE_NUM;

  /**
   * 页码
   */
  @Min(value = 1, message = "pageSize 最小为 1")
  @Max(value = 100, message = "pageSize 最大为 100")
  private Integer pageSize = PageInfoConstant.DEFAULT_PAGE_SIZE;
}
