package com.moose.operator.model.param.poetry;

import com.moose.operator.model.param.SearchParam;
import lombok.Data;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-01-10 15:09:15:09
 * @see com.moose.operator.model.param
 */
@Data
public class PoetrySearchParam extends SearchParam {

  private Integer tagId;
}
