package com.moose.operator.model.param;

import com.moose.operator.annotation.ValueIn;
import com.moose.operator.model.emun.AuthTypeEnum;
import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @author taohua
 */
@Data
public class AuthTypeParam {

  @NotBlank(message = "授权方式不能为空")
  @ValueIn(value = AuthTypeEnum.class, message = "授权方式不正确")
  private String authType;
}
