package com.moose.operator.model.param.book;

import com.moose.operator.model.param.SearchParam;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * @author taohua
 */
@Data
public class HistoryBookParam extends SearchParam {

  @NotNull(message = "书籍分类 Id 不能为空")
  private Long bookCategoryId;
}
