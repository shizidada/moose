package com.moose.operator.model.param;

import com.moose.operator.annotation.ValueIn;
import com.moose.operator.model.emun.LoginTypeEnum;
import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @author taohua
 */
@Data
public class LoginInfoParam {

  /**
   * 账号
   */
  private String accountName;

  /**
   * 密码
   */
  private String password;

  /**
   * 手机号码
   */
  private String phone;

  /**
   * 短信验证码
   */
  private String smsCode;

  /**
   * 授权码登录
   */
  private String code;

  @NotBlank(message = "登录方式不能为空")
  @ValueIn(value = LoginTypeEnum.class, message = "登录方式不正确")
  private String loginType;
}
