package com.moose.operator.model.param;

import lombok.Data;

/**
 * @author taohua
 */
@Data
public class IdiomSearchParam extends SearchParam {
}
