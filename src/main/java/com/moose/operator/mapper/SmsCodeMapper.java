package com.moose.operator.mapper;

import com.moose.operator.model.domain.SmsCodeDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author toahua
 */
@Mapper
public interface SmsCodeMapper {
  /**
   * insert sms code
   *
   * @param smsCodeDO sms code
   */
  void insertSmsCode(SmsCodeDO smsCodeDO);
}
