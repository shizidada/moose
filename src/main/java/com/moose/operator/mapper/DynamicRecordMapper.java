package com.moose.operator.mapper;

import com.moose.operator.model.domain.DynamicRecordDO;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author taohua
 */
@Mapper
public interface DynamicRecordMapper {

  /**
   * insert a dynamic record
   *
   * @param dynamicRecordDO dynamic do
   * @return is success
   */
  boolean insertDynamicRecord(DynamicRecordDO dynamicRecordDO);

  /**
   * select dynamic record by userId
   *
   * @param userId userId
   * @return all record
   */
  List<DynamicRecordDO> selectByUserId(Long userId);

  /**
   * select dynamic record order by createTime
   *
   * @return all record
   */
  List<DynamicRecordDO> selectBaseDynamicRecordInfo();

  /**
   * select dynamic record by dynamicId
   *
   * @param dynamicId dynamicId
   * @return DynamicRecordDO
   */
  DynamicRecordDO selectDynamicRecordByDynamicId(@Param("dynamicId") String dynamicId);

  /**
   * select dynamic record order by createTime
   *
   * @return all record
   */
  List<DynamicRecordDO> selectDynamicRecordWithAssociationInfo();
}
