package com.moose.operator.mapper;

import com.moose.operator.model.domain.AttachmentRecordDO;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author taohua
 */
@Mapper
public interface AttachmentRecordMapper {
  /**
   * select attachment record
   *
   * @param userId   user id
   * @param tag      attachment record eTag
   * @return AttachmentRecordDO
   */
  AttachmentRecordDO selectByUserIdAndEtag(@Param("userId") Long userId,
      @Param("eTag") String tag);

  /**
   * save upload file info
   *
   * @param attachmentRecordDOList List<AttachmentRecordDO>
   * @return is success
   */
  void batchInsertAttachmentRecord(List<AttachmentRecordDO> attachmentRecordDOList);

  /**
   * select attachment record by attachment record id
   *
   * @param attachId attachment record id
   * @return AttachmentRecordDO
   */
  AttachmentRecordDO selectByAttachId(Long attachId);

  /**
   * select attachment file url by attachment id
   *
   * @param attachId attach id
   * @return AttachmentRecordDO
   */
  AttachmentRecordDO selectFileUrlByAttachId(Long attachId);
}
