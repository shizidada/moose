package com.moose.operator.mapper;

import com.moose.operator.model.domain.message.MessageRecordDO;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author taohua
 */

@Mapper
public interface MessageRecordMapper {

  /**
   * 单聊
   * <p>
   * 插入一条单聊消息
   *
   * @param messageRecordDO 消息实体
   * @return 添加成功
   */
  int insertMessageRecord(MessageRecordDO messageRecordDO);

  /**
   * 更新一条记录
   *
   * @param messageRecordDO 消息实体
   * @return 是否更新成功
   */
  int updateByMsgId(MessageRecordDO messageRecordDO);

  /**
   * 查询是否存消息 -
   *
   * @param sendId    发送者 userId
   * @param receiveId 接送者 userId
   * @return MessageRecordDO
   */
  MessageRecordDO selectBySendReceiveId(@Param("sendId") Long sendId,
      @Param("receiveId") Long receiveId);

  /**
   * 聊天列表
   *
   * @param userId 当前用户
   * @return 聊天列表
   */
  List<MessageRecordDO> selectMessageRecordList(Long userId);

  /**
   * 查询聊天列表详情
   *
   * @param sendId    发送者 Id
   * @param receiveId 接收者 Id
   * @return 聊天详情
   */
  List<MessageRecordDO> selectChatMessageList(@Param("sendId") Long sendId,
      @Param("receiveId") Long receiveId);
}
