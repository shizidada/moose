package com.moose.operator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moose.operator.mapper.DynamicRecordAttachmentRelationMapper;
import com.moose.operator.mapper.DynamicRecordMapper;
import com.moose.operator.model.param.SearchParam;
import com.moose.operator.web.service.DynamicRecordService;
import java.util.Map;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author taohua
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class DynamicRecordMapperTest {

  @Resource
  private DynamicRecordMapper dynamicRecordMapper;

  @Resource
  private DynamicRecordAttachmentRelationMapper recordAttachmentRelationMapper;

  @Resource
  private DynamicRecordService dynamicRecordService;

  @Resource
  private ObjectMapper objectMapper;

  @Test
  public void testSelectRecommendDynamicRecord() throws JsonProcessingException {
    //List<DynamicRecordDO> recordDOList = dynamicRecordMapper.selectDynamicRecordWithAssociationInfo();
    //List<DynamicRecordDO> recordDOList = dynamicRecordMapper.selectBaseDynamicRecordInfo();

    //List<DynamicRecordAttachmentRelationDO> attachmentRelationDOS =
    //    recordAttachmentRelationMapper.selectByDynamicRecordId("766462045200580606");

    SearchParam searchParam = new SearchParam();
    Map<String, Object> recommendDynamicRecordList = dynamicRecordService.getRecommendDynamicRecord(
        searchParam);
    log.info("{}", objectMapper.writeValueAsString(recommendDynamicRecordList));
  }
}
