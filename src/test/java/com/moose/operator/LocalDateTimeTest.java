package com.moose.operator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeTest {

  public static void main(String[] args) {
    LocalDateTime now = LocalDateTime.now();
    System.out.println(now);

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMM");

    String format = now.format(formatter);
    System.out.println(formatter);
    System.out.println(format);

    LocalDate date = LocalDate.now();
    System.out.println(date);
    int offset = date.getDayOfMonth() - 1;
    System.out.println(offset);

    System.out.println(now.getSecond());
  }
}
