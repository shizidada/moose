package com.moose.operator.oauth;

import com.moose.operator.mapper.AccountMapper;
import com.moose.operator.mapper.PasswordMapper;
import com.moose.operator.model.domain.AccountDO;
import com.moose.operator.model.domain.PasswordDO;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author taohua
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountMapperTest {

  @Resource
  private AccountMapper accountMapper;

  @Resource
  private PasswordMapper passwordMapper;

  @Test
  public void testInsertAccount() throws Exception {

    AccountDO accountDO = new AccountDO();
    accountDO.setAccountId(98797898213L);
    accountDO.setAccountName("测试用户名");
    accountDO.setStatus("1");
    accountDO.setPhone("1569874569");

    PasswordDO passwordDO = new PasswordDO();
    passwordDO.setAccountId(accountDO.getAccountId());
    passwordDO.setPasswordId(54546714L);
    passwordDO.setPassword("123654789");

    accountMapper.insertAccount(accountDO);
    passwordMapper.insertPassword(passwordDO);
  }
}
