package com.moose.operator.oauth;

import com.moose.operator.util.MapperUtils;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/**
 * @author taohua
 */

@Slf4j
public class GithubTokenTest {

  public static void main(String[] args) {
    String jsonStr =
        "access_token=&scope=&token_type=bearer";
    try {
      Map<String, Object> map = MapperUtils.json2map(jsonStr);
      log.info("{}", map);
    } catch (Exception e) {
      log.error("[{}]", e.getMessage());
    }
  }
}
