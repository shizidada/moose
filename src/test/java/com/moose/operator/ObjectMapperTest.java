package com.moose.operator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moose.operator.common.SnowflakeIdWorker;
import com.moose.operator.model.domain.AccountDO;
import com.moose.operator.model.dto.message.MessageRecordDTO;
import com.moose.operator.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-06-23 12:13:12:13
 * @see com.moose.operator
 */
@Slf4j
public class ObjectMapperTest {

  SnowflakeIdWorker snowflakeIdWorker;

  @Before
  public void runBeforeTestMethod() {
    snowflakeIdWorker = new SnowflakeIdWorker(1, 2);
    log.info("@Before - runBeforeTestMethod");
  }

  @Test
  public void testBase() {
    ObjectMapper mapper = new ObjectMapper();
    AccountDO accountDO = new AccountDO();
    accountDO.setAccountId(snowflakeIdWorker.nextId());
    try {
      String s = mapper.writeValueAsString(accountDO);
      log.info("s {}", s);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testMessageInfo() throws Exception {
    MessageRecordDTO messageInfo = new MessageRecordDTO();
    messageInfo.setContent("hi");
    messageInfo.setMsgId(snowflakeIdWorker.nextId());
    String messageInfoJson = MapperUtils.obj2json(messageInfo);
    // {"msgId":"757308185127157760","content":"hi"}
    log.info(messageInfoJson);
  }
}
