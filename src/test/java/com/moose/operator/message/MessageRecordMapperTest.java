package com.moose.operator.message;

import com.moose.operator.common.SnowflakeIdWorker;
import com.moose.operator.mapper.MessageRecordMapper;
import com.moose.operator.model.domain.message.MessageRecordDO;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author taohua
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageRecordMapperTest {

  @Resource
  private MessageRecordMapper messageRecordMapper;

  @Resource
  private SnowflakeIdWorker snowflakeIdWorker;

  @Test
  public void testInsertMessageRecord() throws Exception {
    MessageRecordDO messageRecordDO = new MessageRecordDO();
    messageRecordDO.setMsgId(snowflakeIdWorker.nextId());
    messageRecordDO.setSendId(775113183131074580L);
    messageRecordDO.setReceiveId(786600935907659776L);
    messageRecordDO.setChatType(1);
    messageRecordDO.setType(1);
    messageRecordDO.setContent("这是一条测试消息");

    int i = messageRecordMapper.insertMessageRecord(messageRecordDO);
    log.info("{}", i);
  }

  @Test
  public void testSelectBySendReceiveId() throws Exception {
    Long sendId = 775113183131074580L;
    Long receiveId = 786600935907659776L;
    MessageRecordDO messageRecordDO = messageRecordMapper.selectBySendReceiveId(sendId, receiveId);
    log.info("{}", messageRecordDO);
  }

  @Test
  public void testSelectMessageRecordList() {
    // 江景
    Long userId = 775113183131074580L;
    List<MessageRecordDO> messageRecordDOS = messageRecordMapper.selectMessageRecordList(userId);
    log.info("{}", messageRecordDOS);
  }


  @Test
  public void testSelectChatMessageList() {
    Long sendId = 775113183131074580L;
    Long receiveId = 786600935907659776L;
    List<MessageRecordDO> messageRecordDOS = messageRecordMapper.selectChatMessageList(sendId, receiveId);
    log.info("{}", messageRecordDOS);
  }
}
