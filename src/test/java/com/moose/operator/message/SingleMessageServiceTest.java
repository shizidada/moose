package com.moose.operator.message;

import com.moose.operator.model.template.MessageTemplate;
import com.moose.operator.web.service.SingleMessageService;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author taohua
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class SingleMessageServiceTest {

  @Resource
  private SingleMessageService singleMessageService;

  @Test
  public void testSaveMessage() {
    MessageTemplate template = new MessageTemplate();
    mockJiangJing(template);
    mockTaohua(template);
    mockWangZhaoJun(template);
  }

  private void mockWangZhaoJun(MessageTemplate template) {
    // 王昭君
    template.setSendId(775113183131074560L);
    // 江景
    template.setReceiveId(775113183131074580L);
    template.setType("MS:TEXT");
    template.setChatType("CT:SINGLE");
    template.setContent("你好，江景，我是王昭君。");
    singleMessageService.saveMessage(template);
    // 桃花
    template.setReceiveId(786600935907659776L);
    template.setType("MS:TEXT");
    template.setChatType("CT:SINGLE");
    template.setContent("你好，桃花，我是王昭君。");
    singleMessageService.saveMessage(template);
  }

  private void mockTaohua(MessageTemplate template) {

    // 桃花
    template.setSendId(786600935907659776L);
    // 王昭君
    template.setReceiveId(775113183131074560L);
    template.setType("MS:TEXT");
    template.setChatType("CT:SINGLE");
    template.setContent("你好，王昭君，我是桃花。");
    singleMessageService.saveMessage(template);

    // 江景
    template.setReceiveId(775113183131074580L);
    template.setType("MS:TEXT");
    template.setChatType("CT:SINGLE");
    template.setContent("你好，江景，我是桃花。");
    singleMessageService.saveMessage(template);

    // 苏轼
    template.setReceiveId(801901011592810500L);
    template.setType("MS:TEXT");
    template.setChatType("CT:SINGLE");
    template.setContent("你好，苏轼，我是桃花。");
    singleMessageService.saveMessage(template);
  }

  private void mockJiangJing(MessageTemplate template) {

    // 江景
    template.setSendId(775113183131074580L);

    // 桃花
    template.setReceiveId(786600935907659776L);
    template.setType("MS:TEXT");
    template.setChatType("CT:SINGLE");
    template.setContent("你好，桃花，我是江景。");
    singleMessageService.saveMessage(template);

    // 王昭君
    template.setReceiveId(775113183131074560L);
    template.setType("MS:TEXT");
    template.setChatType("CT:SINGLE");
    template.setContent("你好，王昭君，我是江景。");
    singleMessageService.saveMessage(template);
  }
}
