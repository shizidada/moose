package com.moose.operator.poetry;

import com.moose.operator.common.PageInfo;
import com.moose.operator.model.entity.PoetryTagInfo;
import com.moose.operator.model.param.SearchParam;
import com.moose.operator.web.service.PoetryTagInfoService;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author taohua
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class PoetryTagInfoServiceTest {

  @Resource
  private PoetryTagInfoService poetryTagInfoService;

  @Resource
  private MongoTemplate mongoTemplate;

  @Test
  public void testPoetryTag() {
    SearchParam searchParam = new SearchParam();
    PageInfo<PoetryTagInfo> poetryTagInfoPageInfo = poetryTagInfoService.listPoetryTag(searchParam);
    log.info("{}", poetryTagInfoPageInfo);
  }

  @Test
  public void testPoetryTagTotal() {
    Aggregation aggregation =
        Aggregation.newAggregation(Aggregation.group("_id").sum("count").as("total"));
    AggregationResults<Document> poetryTagInfo =
        mongoTemplate.aggregate(aggregation, "poetry_tag_info", Document.class);
    log.info("{}", poetryTagInfo);
  }
}
