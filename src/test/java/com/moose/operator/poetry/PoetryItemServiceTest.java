package com.moose.operator.poetry;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moose.operator.common.PageInfo;
import com.moose.operator.model.entity.PoetryItem;
import com.moose.operator.model.param.poetry.PoetrySearchParam;
import com.moose.operator.web.service.PoetryItemService;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-13 21:50:21:50
 * @see com.moose.operator
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class PoetryItemServiceTest {

  @Resource
  private ObjectMapper objectMapper;

  @Resource
  private PoetryItemService poetryItemService;

  @Resource
  private MongoTemplate mongoTemplate;

  @Test
  public void testListPoetry() throws JsonProcessingException {
    PageInfo<PoetryItem> poetryItemPageInfo = poetryItemService.listPoetry(new PoetrySearchParam());
    log.info("{}", objectMapper.writeValueAsString(poetryItemPageInfo));
  }

  @Test
  public void testFindTagArrTagId() throws JsonProcessingException {
    Criteria where = Criteria.where("tag_arr.id").is(279);
    Query query = Query.query(where);
    List<PoetryItem> poetryItems = mongoTemplate.find(query, PoetryItem.class);
    log.info("{}", objectMapper.writeValueAsString(poetryItems));
  }
}
