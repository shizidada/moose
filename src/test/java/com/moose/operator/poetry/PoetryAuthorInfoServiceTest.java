package com.moose.operator.poetry;

import com.moose.operator.common.PageInfo;
import com.moose.operator.model.entity.PoetryAuthorInfo;
import com.moose.operator.model.param.SearchParam;
import com.moose.operator.web.service.PoetryAuthorInfoService;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author taohua
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class PoetryAuthorInfoServiceTest {

  @Resource
  private PoetryAuthorInfoService poetryAuthorInfoService;

  @Test
  public void testListPoetryAuthorInfo() {
    SearchParam searchParam = new SearchParam();

    PageInfo<PoetryAuthorInfo> pageInfo = poetryAuthorInfoService.listPoetryAuthorInfo(searchParam);
    log.info("{}", pageInfo);
  }
}