package com.moose.operator.poetry;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.moose.operator.common.PageInfo;
import com.moose.operator.model.entity.PoetryItem;
import com.moose.operator.model.param.SearchParam;
import com.moose.operator.web.service.PoetryItemService;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2020-12-06 22:24:22:24
 * @see com.moose.operator
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class PoetryItemTest {

  @Resource
  private ObjectMapper objectMapper;

  @Resource
  private MongoTemplate mongoTemplate;

  @Resource
  private PoetryItemService poetryItemService;

  @Test
  public void testSave() {
    PoetryItem poetryItem = new PoetryItem();
    poetryItem.setAuthor("好诗词");
    poetryItem.setBody("今天不开心就止于此吧明天依旧光芒万丈");
    // TODO: setAuthorId
    //poetryItem.setAuthorId("江景");
    poetryItem.setCreateTime(new Date().getTime());
    poetryItem.setUpdateTime(new Date().getTime());
    PoetryItem save = mongoTemplate.save(poetryItem);
    log.info("{}", save);
  }

  @Test
  public void testDelete() {
    Query query = Query.query(Criteria.where("poetry_author").is("江景"));
    DeleteResult remove = mongoTemplate.remove(query, PoetryItem.class);
    log.info("{}", remove);
  }

  @Test
  public void testUpdate() {
    Query query = Query.query(Criteria.where("poetry_author").is("江景"));
    Update update = Update.update("poetry_name", "《好诗，好诗》");
    UpdateResult updateResult = mongoTemplate.updateFirst(query, update, PoetryItem.class);
    log.info("{}", updateResult);
  }

  @Test
  public void testUpdateFirst() {
    Query query = Query.query(new Criteria());
    Update update = Update.update("poetry_name", "江景");
    UpdateResult updateResult = mongoTemplate.updateFirst(query, update, PoetryItem.class);
    log.info("{}", updateResult);
  }

  @Test
  public void testUpdateMulti() {
    Query query = Query.query(new Criteria());
    Update update = Update.update("poetry_name", "江景");
    UpdateResult updateResult = mongoTemplate.updateMulti(query, update, PoetryItem.class);
    log.info("{}", updateResult);
  }

  @Test
  public void testFindOne() {
    Criteria criteria = Criteria.where("poetry_author").is("江景");
    Query query = Query.query(criteria);
    PoetryItem poetryItem = mongoTemplate.findOne(query, PoetryItem.class);
    log.info("{}", poetryItem);
  }

  @Test
  public void testFindOne2() {
    Query query = Query.query(Criteria.where("poetry_author").is("李白"));
    PoetryItem poetryItem = mongoTemplate.findOne(query, PoetryItem.class);
    log.info("{}", poetryItem);
  }

  @Test
  public void testFindOne3() {
    Query query = Query.query(
        Criteria.where("poetry_author").is("李白")
            .and("poetry_name").is("《将进酒·君不见黄河之水天上来》"));
    PoetryItem poetryItem = mongoTemplate.findOne(query, PoetryItem.class);
    log.info("{}", poetryItem);
  }

  @Test
  public void testFindAll() {
    Query query = Query.query(new Criteria());
    List<PoetryItem> allPoetry = mongoTemplate.find(query, PoetryItem.class);
    log.info("{}", allPoetry);
  }

  @Test
  public void testFindAll2() {
    Query query = Query.query(Criteria.where("poetry_author").is("李白"));
    List<PoetryItem> allPoetry = mongoTemplate.find(query, PoetryItem.class);
    log.info("{}", allPoetry);
  }

  @Test
  public void testFindAll3() {
    List<PoetryItem> allPoetry = mongoTemplate.findAll(PoetryItem.class);
    log.info("{}", allPoetry);
  }

  @Test
  public void testFindDistinct() {
    /**
     * String field, 查询字段
     * Class<?> entityClass, 查询映射类型
     * Class<T> resultClass 查询返回结果类型
     */
    List<String> poetryAuthor =
        mongoTemplate.findDistinct("poetry_author", PoetryItem.class, String.class);
    log.info("{}", poetryAuthor);
  }

  @Test
  public void testFindLimit() {
    Criteria criteria = new Criteria();
    Query query = Query.query(criteria);

    // 方式一
    query.skip(0).limit(10).with(Sort.by(Sort.Direction.DESC, "poetry_num"));

    // 方式二
    //Pageable pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "poetry_num"));
    //query.with(pageable);

    List<PoetryItem> poetryEntities = mongoTemplate.find(query, PoetryItem.class);
    log.info("{}", poetryEntities);
  }

  @Test
  public void testAggregation() throws JsonProcessingException {
    //List<AggregationOperation> operations = new ArrayList<>();
    //operations.add(Aggregation.match(Criteria.where("poetry_author").is("李白")));
    //operations.add(Aggregation.group("poetry_author").sum());
    //operations.add(Aggregation.group("poetry_author").sum("poetryAuthor").as("poetryAuthor"));

    //operations.add(Aggregation.match(Criteria.where("poetry_author").is("李白")));
    //operations.add(Aggregation.group("poetry_author").count().as("count"));
    //log.info("{}", objectMapper.writeValueAsString(operations));

    Aggregation aggregation =
        Aggregation.newAggregation(
            //Aggregation.match(Criteria.where("poetry_num").is("2")),
            Aggregation.group("poetry_author").count().as("totalCount"),

            // 根据分组的字段做别名，mongodb 查询的默认在 _id 中
            Aggregation.project("poetry_author", "totalCount")
                .and("poetryAuthor")
                .previousOperation()
            //, Aggregation.limit(10)
        );

    log.info("{}", aggregation.toString());

    AggregationResults<Document> poetryAggregation =
        mongoTemplate.aggregate(aggregation, "poetry_item", Document.class);
    List<Document> mappedResults = poetryAggregation.getMappedResults();
    for (Document item : mappedResults) {
      log.info("{}", objectMapper.writeValueAsString(item));
    }
  }

  @Test
  public void testCount() {
    Criteria criteria = new Criteria();
    Query query = Query.query(criteria);
    long count = mongoTemplate.count(query, PoetryItem.class);
    log.info("{}", count);
  }

  @Test
  public void testA() {
    Aggregation aggregation =
        Aggregation.newAggregation(
            Aggregation.match(Criteria.where("id").is(18489)),
            Aggregation.project("body")
        );
    AggregationResults<Document> aggregate =
        mongoTemplate.aggregate(aggregation, "poetry_item", Document.class);
    List<Document> mappedResults = aggregate.getMappedResults();
    log.info("{}", mappedResults);
  }

  @Test
  public void testPoetryRankingList() {
    PageInfo<PoetryItem> poetryRankings = poetryItemService.listPoetryRanking(new SearchParam());
    log.info("{}", poetryRankings);
  }
}
