package com.moose.operator;

import com.moose.operator.util.OSSClientUtils;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author taohua
 * @version v1.0.0
 * @date 2021-03-20 20:17:20:17
 * @see com.moose.operator
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class OOSClientTest {

  @Resource
  private OSSClientUtils ossClientUtils;

  @Test
  public void testSetObjectAclPublicRead() {
    String key = "775113183131074580/2021-03-20/temp.jpg";
    ossClientUtils.setObjectAclPublicRead(key);
  }
}
