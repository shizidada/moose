package com.moose.operator;

import lombok.extern.slf4j.Slf4j;

/**
 * @author taohua
 */

@Slf4j
public class PatternTest {

  public static void main(String[] args) {
    String body =
        "<p>　　谊为长沙王太傅，既以谪<span>(zhé)</span>去，意不自得；及度湘水，为赋<span>(fù)</span>以吊屈原。屈原，楚贤臣也。被谗<span>(chán)</span>放逐，作《离骚》赋，其终篇曰：“已矣哉！国无人兮，莫我知也。”遂自投汨<span>(mì)</span>罗而死。谊追伤之，因自喻，其辞曰：</p><p>　　恭承嘉惠兮，俟<span>(sì)</span>罪长沙；侧闻屈原兮，自沉汨罗。造讬<span>(tuō)</span>湘流兮，敬吊先生；遭世罔<span>(wǎng)</span>极兮，乃殒<span>(yǔn)</span>厥<span>(jué)</span>身。呜呼哀哉！逢时不祥。鸾<span>(luán)</span>凤伏竄<span>(cuàn)</span>兮，鸱<span>(chī)</span>枭<span>(xiāo)</span>翱<span>(áo)</span>翔。闒<span>(tà)</span>茸尊显兮，谗谀<span>(yú)</span>得志；贤圣逆曳<span>(yè)</span>兮，方正倒植。世谓随、夷为溷<span>(hún)</span>兮，谓跖<span>(zhí)</span>、蹻<span>(jué)</span>为廉；莫邪<span>(yé)</span>为钝兮，铅刀为銛<span>(xiān)</span>。吁<span>(xū)</span>嗟<span>(jiē)</span>默默，生之无故兮；斡<span>(wò)</span>弃周鼎，宝康瓠<span>(hù)</span>兮。腾驾罷<span>(pí)</span>牛，骖<span>(cān)</span>蹇<span>(jiǎn)</span>驴兮；骥<span>(jì)</span>垂两耳，服盐车兮。章甫荐履<span>(lǚ)</span>，渐不可久兮；嗟苦先生，独离此咎<span>(jiù)</span>兮。</p><p>　　讯曰：已矣！国其莫我知兮，独壹<span>(yī)</span>郁<span>(yù)</span>其谁语？凤漂<span>(piāo)</span>漂其高逝兮，固自引而远去。袭九渊之神龙兮，沕<span>(wù)</span>深潜以自珍；偭<span>(miǎn)</span>蟂<span>(xiāo)</span>獭<span>(tǎ)</span>以隐处兮，夫岂从虾<span>(há)</span>与蛭<span>(zhì)</span>蟥<span>(huáng)</span>？所贵圣人之神德兮，远浊世而自藏；使骐<span>(qí)</span>骥<span>(jì)</span>可得系而羁<span>(jī)</span>兮，岂云异夫犬羊？般纷纷其离此尤兮，亦夫子之故也。历九州而其君兮，何必怀此都也？凤凰翔于千仞兮，览德辉而下之；见细德之险徵兮，遥曾击而去之。彼寻常之污渎兮，岂能容夫吞舟之巨鱼？横江湖之鳣<span>(zhān)</span>鲸<span>(jīng)</span>兮，固将制于蝼蚁。</p>";
    String replace = body.replaceAll("\\((.+?\\))", "").replaceAll("<[^>]+>", "");
    log.info(replace);
  }
}
