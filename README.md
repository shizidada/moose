## moose operator service

## run project
- 初始化数据库 src/main/resources/moose.sql
- 配置 mongodb/redis
- 运行主启动类 MooseOperatorApplication
- 访问 http://localhost:7000

## application-dev.yml
![application-dev.yml](https://gitee.com/shizidada/moose-resource/raw/master/blog/20210224111453.png)

## TODO
- [x] 登录方式校验 (AOP ValueIn annotation) 
- [x] 是否频繁调用登录接口
- [x] 集成 netty-socketio
- [ ] 判断是否已经登录，在调用登录接口时，应直接返回登录信息


![公众号二维码](https://gitee.com/shizidada/moose/raw/master/images/wechat%20qrcode.png)